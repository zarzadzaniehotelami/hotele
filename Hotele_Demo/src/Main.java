import classes.Hotel;
import classes.Room;
import classes.*;
import database.Database;
import windows.MainPanel;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main  extends JFrame {

    public static void createAndShowGui(Database db)
    {
        JFrame frame = new JFrame("HOTELS");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        MainPanel panel = new MainPanel(db); //glowny panel, ktory bedzie laczyl w sobie wszystkie pozostale okna, przelaczal
                                           //w zaleznosci od potrzeby
        panel.setVisible(true);
        frame.setContentPane(panel); //zawiera ten panel glowny

        frame.pack(); //ustawia rozmiar w zaleznosci od komponentow
        frame.setVisible(true); //zeby bylo widoczne
        frame.setResizable(true); //rozciaganie okna 
    }
    public static void main(String[] args) {

        Database database = Database.getInstance();

        /*database.insertUser(new User(0,"Admin", "Admin", "Root", "Root", null, null, null));
        database.insertUser(new User(0,"Adam", "Kowalski", "kowal", "password",
                "123456789", "kow@gmail.com", "Client"));

        database.insertHotel(new Hotel(0, "Hotel1", "Krakow", 200, "sport, kregle,spa",
                "danie1, danie2, danie3", "beverage1, beverage2, beverage3", true, 1));

        database.insertHotel(new Hotel(0, "Hotel2", "Poznan", 200, "sport, kregle,spa",
                "danie1, danie2, danie3", "beverage1, beverage2, beverage3", true, 1));

        database.insertRoom(new Room(0, 1, 250, 3, 300, true));
        database.insertRoom(new Room(0, 2, 251, 2, 300, true));*/


        //new ClientAccountWindow(null);
        //database.insertUser(new User(0,"Adam", "Kowalski", "aaEEa", "as", "123", "as", "Klient"));
        /*database.deleteUser(10);
        for (int i=0; i<database.selectUser().size(); i++) {
            System.out.println(database.selectUser().get(i));
        }*/
        /*
        database.insertUser(new User(0,"Adam", "Kowalski", "kowal", "password",
                "123456789", "kow@gmail.com", "Client"));


        database.insertConferenceRoom(new ConferenceRoom(0, 100));
        database.insertHotel(new Hotel(0, "Hotel1", "Krakow", 200, "sport, kregle,spa",
         "danie1, danie2, danie3", "beverage1, beverage2, beverage3", true, 1));

        database.insertHotel(new Hotel(0, "Hotel2", "Poznan", 200, "sport, kregle,spa",
                "danie1, danie2, danie3", "beverage1, beverage2, beverage3", true, 1));

        database.insertRoom(new Room(0, 1, 250, 3, 300, true));
        database.insertRoom(new Room(0, 2, 251, 2, 300, true));

        System.out.println(database.selectUserByLogin("kowal").getPhoneNumber());
        System.out.println(database.selectHotelById(1).getName());

*/
//        database.insertUser(new User(0,"Kasia", "Nowak", "Employee", "Employee",
//                "163556789", "nowakas@gmail.com", "Employee"));
//        System.out.println(database.selectUserByLogin("Employee"));

//        database.insertRoom(new Room(0, 2, 111, 2, 350, false));
//        database.insertRoom(new Room(0, 2, 51, 4, 450, true));

//        database.insertUser(new User(0, "Adam", "Pawłowicz", "Manager", "Manager", "765435234", "pawlowadam@gmail.com", "manager"));

 //       System.out.println(database.getUserStatus("Employee"));

        Date date= Calendar.getInstance().getTime();
        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate=dateFormat.format(date);
        String name = "raport-" + formattedDate + ".pdf";
        System.out.println(name);


        javax.swing.SwingUtilities.invokeLater(
                new Runnable()
                {
                    public void run()
                    {
                        createAndShowGui(database);
                    }
                }
        );
    }
}
