package classes;

import java.time.LocalDate;

public class RoomBooking {

        private int idBookingRoom;
        private int idUser;
        private int idRoom;
        private int numberOfPeople;
        private double price;
        private LocalDate arrivalDate;
        private LocalDate departureDate;

        public RoomBooking(int idBookingRoom, int idUser, int idRoom, int numberOfPeople, double price, LocalDate arrivalDate, LocalDate departureDate) {
            this.idBookingRoom = idBookingRoom;
            this.idUser = idUser;
            this.idRoom = idRoom;
            this.numberOfPeople = numberOfPeople;
            this.price = price;
            this.arrivalDate = arrivalDate;
            this.departureDate = departureDate;
        }

        public int getIdUser() {
            return idUser;
        }

        public void setIdUser(int idUser) {
            this.idUser = idUser;
        }

        public int getIdRoom() {
            return idRoom;
        }

        public void setIdRoom(int idRoom) {
            this.idRoom = idRoom;
        }

        public int getNumberOfPeople() {
            return numberOfPeople;
        }

        public void setNumberOfPeople(int numberOfPeople) {
            this.numberOfPeople = numberOfPeople;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public LocalDate getArrivalDate() {
            return arrivalDate;
        }

        public void setArrivalDate(LocalDate arrivalDate) {
            this.arrivalDate = arrivalDate;
        }

        public LocalDate getDepartureDate() {
            return departureDate;
        }

        public void setDepartureDate(LocalDate departureDate) {
            this.departureDate = departureDate;
        }

        public int getIdBookingRoom() {
            return idBookingRoom;
        }

        public void setIdBookingRoom(int idBookingRoom) {
            this.idBookingRoom = idBookingRoom;
        }

        @Override
        public String toString() {
            return "RoomBooking{" +
                    "idBookingRoom=" + idBookingRoom +
                    ", idUser=" + idUser +
                    ", idRoom=" + idRoom +
                    ", numberOfPeople=" + numberOfPeople +
                    ", price=" + price +
                    ", arrivalDate=" + arrivalDate +
                    ", departureDate=" + departureDate +
                    '}';
        }

    }



