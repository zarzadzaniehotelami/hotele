package classes;

public class Room {
    private int idRoom;
    private int idHotel;
    private int roomNumber;
    private int numberOfPeople;
    private int price;
    private boolean ifAvailable;

    public Room(int idRoom, int idHotel, int roomNumber, int numberOfPeople, int price, boolean ifAvailable) {
        this.idRoom = idRoom;
        this.idHotel = idHotel;
        this.roomNumber = roomNumber;
        this.numberOfPeople = numberOfPeople;
        this.price = price;
        this.ifAvailable = ifAvailable;
    }

    public int getNumberOfFields() {
        return 6;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean getIfAvailable() {
        return ifAvailable;
    }

    public void setIfAvailable(boolean ifAvailable) {
        this.ifAvailable = ifAvailable;
    }

    @Override
    public String toString() {
        return "Room{" +
                "idRoom=" + idRoom +
                ", idHotel=" + idHotel +
                ", roomNumber=" + roomNumber +
                ", numberOfPeople=" + numberOfPeople +
                ", price=" + price +
                ", ifAvailable=" + ifAvailable +
                '}';
    }
}
