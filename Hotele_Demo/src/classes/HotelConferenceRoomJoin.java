package classes;

public class HotelConferenceRoomJoin {

    private String name;
    private String city;
    private int size;

    public HotelConferenceRoomJoin(String name, String city, int size) {
        this.name = name;
        this.city = city;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
