package classes;

public class HotelRoomJoin {

    private String name;
    private String city;
    private String relaxationRoom;
    private String menuMeals;
    private String menuDrinks;
    private Boolean isParking;
    private int idRoom;
    private int roomNumber;
    private int numberOfPeople;
    private Boolean isAvailable;
    private int price;

    public HotelRoomJoin(String name, String city, String relaxationRoom, String menuMeals, String menuDrinks, Boolean isParking, int idRoom, int roomNumber, int numberOfPeople, Boolean isAvailable, int price) {
        this.name = name;
        this.city = city;
        this.relaxationRoom = relaxationRoom;
        this.menuMeals = menuMeals;
        this.menuDrinks = menuDrinks;
        this.isParking = isParking;
        this.idRoom = idRoom;
        this.roomNumber = roomNumber;
        this.numberOfPeople = numberOfPeople;
        this.isAvailable = isAvailable;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRelaxationRoom() {
        return relaxationRoom;
    }

    public void setRelaxationRoom(String relaxationRoom) {
        this.relaxationRoom = relaxationRoom;
    }

    public String getMenuMeals() {
        return menuMeals;
    }

    public void setMenuMeals(String menuMeals) {
        this.menuMeals = menuMeals;
    }

    public String getMenuDrinks() {
        return menuDrinks;
    }

    public void setMenuDrinks(String menuDrinks) {
        this.menuDrinks = menuDrinks;
    }

    public Boolean getParking() {
        return isParking;
    }

    public void setParking(Boolean parking) {
        isParking = parking;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    @Override
    public String toString() {
        return "HotelRoomJoin{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", relaxationRoom='" + relaxationRoom + '\'' +
                ", menuMeals='" + menuMeals + '\'' +
                ", menuDrinks='" + menuDrinks + '\'' +
                ", isParking=" + isParking +
                ", roomNumber=" + roomNumber +
                ", numberOfPeople=" + numberOfPeople +
                ", isAvailable=" + isAvailable +
                ", price=" + price +
                '}';
    }
}
