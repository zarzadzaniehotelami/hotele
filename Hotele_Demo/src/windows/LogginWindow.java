package windows;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.sql.SQLException;

import classes.User;
import database.*;
import windows.Admin.AdminAccountWindow;
import windows.Customer.ClientAccountWindow;
import windows.Customer.ClientFormWindow;

public class LogginWindow extends JPanel {
    private Database db;
    private JPanel MainPanel;
    private JTextField login_field;
    private JTextField passwd_field;

    private ClientFormWindow clientFormWindow;

    /*public LoggingWindow() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Logging Window");
        setLayout(new FlowLayout());
        CreateComponents();
        setVisible(true);
        pack();
    }*/

    public LogginWindow(Database _db) {
        db=_db;
        CreateComponents();
    }

    public void CreateComponents() {

        //JPanel panel = new JPanel(new CardLayout());

        MainPanel = new JPanel();
        MainPanel.setLayout(new GridLayout(3, 5));
        MainPanel.setSize(new Dimension(200, 220));
        MainPanel.setBorder(new LineBorder(Color.BLACK));
        MainPanel.setVisible(true);
        add(MainPanel);
        JLabel login_label = new JLabel("Login");
        MainPanel.add(login_label);
        login_field = new JTextField(20);
        MainPanel.add(login_field);
        JLabel passwd_label = new JLabel("Hasło");
        MainPanel.add(passwd_label);
        passwd_field = new JPasswordField(20);
        MainPanel.add(passwd_field);
        JButton submit_btn = new JButton("Wejdź");
        submit_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Authorization(login_field.getText(), passwd_field.getText());
            }
        });
        MainPanel.add(submit_btn);
        JButton register_btn = new JButton("Zarejestruj się");
        register_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ClientFormWindow(db);
            }
        });
        /*register_btn.addActionListener(e -> {
            CardLayout cardLayout = (CardLayout) (this.getLayout());
            cardLayout.show(this, "clientForm");
        });*/
        MainPanel.add(register_btn);


        //panel.add(MainPanel);
        //clientFormWindow = new ClientFormWindow();
        //add(clientFormWindow, "clientForm");
    }

    public boolean Authorization(String Login, String Password) {

        String status = db.getUserStatus(Login);

        if (Login.equals("Root") && Password.equals("Root")) {
            //admin account
            JOptionPane.showMessageDialog(null, String.format("Jesteś zalogowany jako ADMIN"));
            new AdminAccountWindow(db);
            System.out.println("Admin");
            return true;
        }
//        else if (Login.equals("Employee") && Password.equals("Employee")) {
        else if (status.equals("Employee")) {
            JOptionPane.showMessageDialog(null, "Jesteś zalogowany jako PRACOWNIK");
            new selectHotelWindow("Employee", db);
//            System.out.println("Employee");
            return true;
        }
        else if (Login.equals("Manager") && Password.equals("Manager")) {
            JOptionPane.showMessageDialog(null, "Jesteś zalogowany jako KIEROWNIK");
            new selectHotelWindow("Manager", db);
            System.out.println("Manager");
            return true;
        }
        boolean logged=false;
        String query;
        try {
            Connection connection=db.getConnection();
            query="SELECT * FROM User WHERE login=? AND password=?;";
            PreparedStatement preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE);
            preparedStatement.setString(1, Login);
            preparedStatement.setString(2, Password);
            ResultSet resultSet=preparedStatement.executeQuery();
            //System.out.println(new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8)));
            if (resultSet.next()) {
                logged=true;
            }
            if(logged == true) {
                JOptionPane.showMessageDialog(null, "Zostałeś pomyślnie zalogowany");
                User user = new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8));
                JOptionPane.showMessageDialog(null, "Hello " + user.getName());
                //przejście do okna klienta

                new ClientAccountWindow(Login, db);
            }
            else {
                JOptionPane.showMessageDialog(null,"Nie jesteś zarejestrowany");
                //przejscie do okna rejestracji
                new ClientFormWindow(db);
            }

        } catch( SQLException e) {
            e.printStackTrace();
        }
        return logged;
    }
}
