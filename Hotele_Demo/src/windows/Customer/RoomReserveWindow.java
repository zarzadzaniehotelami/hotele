package windows.Customer;

import classes.HotelRoomJoin;
import database.Database;
import org.jdesktop.swingx.JXDatePicker;
import windows.Customer.AvailableRoomsWindow;
import windows.Customer.ClientAccountWindow;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pawelmrozikowski on 04.05.2017.
 */
public class RoomReserveWindow extends JFrame {
    private JButton back;
    private JLabel whichCityLable;
    private JTextField whichCityText;
    private JLabel checkInLabel;
    private JXDatePicker checkInDate;
    private JLabel checkOutLabel;
    private JXDatePicker checkOutDate;
    private JLabel adultsLabel;
    private JComboBox adultsComboBox;
    private JLabel childrenLabel;
    private JComboBox childrenComboBox;
    private JButton search;

    private List<HotelRoomJoin> roomList;
    private Database database;
    private List<HotelRoomJoin> choosenRooms;

    private ClientAccountWindow clientAccountWindow;
    private AvailableRoomsWindow availableRoomsWindow;

    public RoomReserveWindow(ClientAccountWindow clientAccountWindow) {
        setTitle("Reserve conference room");
        setLayout(new FlowLayout());
        setSize(500, 400);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        //pack();
        this.clientAccountWindow = clientAccountWindow;
        availableRoomsWindow = new AvailableRoomsWindow(this);
        createComponents();
    }

    public void createComponents(){

        JPanel mainPanel = new JPanel(new CardLayout());
        JPanel searchPanel = new JPanel(new GridBagLayout());

        database = Database.getInstance();
        roomList = new LinkedList<>();
        roomList = database.innerJoinHotelRoom();
        choosenRooms = new LinkedList<>();

        GridBagConstraints gb = new GridBagConstraints();

        back = new JButton("<");
        back.setPreferredSize(new Dimension(80,20));
        back.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 14));
        gb.gridx = 0;
        gb.gridy = 0;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(back, gb);
        back.addActionListener(e -> {
            clientAccountWindow.setVisible(true);
            this.setVisible(false);
        });

        whichCityLable = new JLabel("City");
        gb.gridx = 0;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(whichCityLable, gb);

        whichCityText = new JTextField(10);
        gb.gridx = 1;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(whichCityText, gb);

        checkInLabel = new JLabel("Check in");
        gb.gridx = 0;
        gb.gridy = 2;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(checkInLabel, gb);

        checkInDate = new JXDatePicker();
        checkInDate.setDate(Calendar.getInstance().getTime());
        checkInDate.setFormats(new SimpleDateFormat("dd.MM.yyyy"));
        gb.gridx = 1;
        gb.gridy = 2;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(checkInDate, gb);

        checkOutLabel = new JLabel("Check out");
        gb.gridx = 0;
        gb.gridy = 3;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(checkOutLabel, gb);

        checkOutDate = new JXDatePicker();
        checkOutDate.setDate(Calendar.getInstance().getTime());
        checkOutDate.setFormats(new SimpleDateFormat("dd.MM.yyyy"));
        gb.gridx = 1;
        gb.gridy = 3;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(checkOutDate, gb);

        adultsLabel = new JLabel("Adults");
        gb.gridx = 0;
        gb.gridy = 4;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(adultsLabel, gb);

        adultsComboBox = new JComboBox();
        for(int i = 1; i < 10; i++){
            adultsComboBox.addItem(i);
        }
        gb.gridx = 1;
        gb.gridy = 4;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(adultsComboBox, gb);

        childrenLabel = new JLabel("Children");
        gb.gridx = 0;
        gb.gridy = 5;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(childrenLabel, gb);

        childrenComboBox = new JComboBox();
        for(int i = 0; i < 10; i++){
            childrenComboBox.addItem(i);
        }
        gb.gridx = 1;
        gb.gridy = 5;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(childrenComboBox, gb);


        search = new JButton("search");
        gb.gridx = 0;
        gb.gridy = 6;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        searchPanel.add(search, gb);
        search.addActionListener(e -> {


            //pobieranie zawartosci z pola comboboxa, ile osob doroslych
            int numberOfPeople = (int)adultsComboBox.getSelectedItem();

            //filtrowanie listy na podstawie podanego przez klienta miasta i ilosci osob
            if(!whichCityText.getText().isEmpty()) {
                choosenRooms = roomList.stream()
                        .filter(r -> r.getCity().equals(whichCityText.getText().trim()))
                        .filter(r -> r.getAvailable())
                        .collect(Collectors.toList());
                availableRoomsWindow.updateTableModel(choosenRooms);
                CardLayout cardLayout = (CardLayout) (mainPanel.getLayout());
                cardLayout.show(mainPanel,"roomsWindow");
            }
            else{
                JOptionPane.showMessageDialog(null, "Please choose the city");
            }

        });

        mainPanel.add(searchPanel, "main");
        mainPanel.add(availableRoomsWindow, "roomsWindow");

        add(mainPanel);
    }

    public String getNameOfCity(){
        return whichCityText.getText().trim();
    }

    public List<HotelRoomJoin> getChoosenRooms() {
        return choosenRooms;
    }

    public JTextField getWhichCityText() {
        return whichCityText;
    }

    public JComboBox getAdultsComboBox() {
        return adultsComboBox;
    }

    public int getNumberOfDays(){
        LocalDate dateCheckIn = checkInDate.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate dateCheckOut = checkOutDate.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Period period = Period.between (dateCheckIn, dateCheckOut);

        return period.getDays();
    }

    public int getNumberOfPeople(){
        return (int)adultsComboBox.getSelectedItem();
    }

    public String getUserName(){
        return clientAccountWindow.getUsername();
    }

    public LocalDate getArrivaleDate(){
        return checkInDate.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public LocalDate getDepartureDate(){
        return checkOutDate.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
}