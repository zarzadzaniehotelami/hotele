package windows.Customer;

import classes.User;
import database.Database;
import others.ValidationController;
import others.ValidationType;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.UUID;

public class ClientFormWindow extends JFrame {
    private Database db;
    private JPanel ContentPanel;
    private JTextField fields[];
    private JPasswordField passwd;
    private boolean ErrorDetected;

    public ClientFormWindow(Database _db) {
        db=_db;
        ErrorDetected=false;
        setBounds(100, 100, 400, 800);
        setTitle("Rejestracja");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new FlowLayout());
        setVisible(true);
        ContentPanel=new JPanel();
        ContentPanel.setLayout(new GridLayout(7, 3));
        setBounds(110, 110, 500, 300);
        ContentPanel.setBorder(new LineBorder(Color.BLACK));
        ContentPanel.setVisible(true);
        setVisible(true);
        add(ContentPanel);
        PrepareComponents();
        pack();
    }


    public void paintComponent(Graphics g) {
        super.paintComponents(g);
    }

    public void PrepareComponents() {
        JLabel labels[]=new JLabel[6];
        fields=new JTextField[5];
        passwd=new JPasswordField();
        String labelnames[]={"Imię: *", "Nazwisko: *", "Login: *", "Hasło: *", "Numer kontaktowy: *", "Adres email: *"};
        for (int i=0; i<6; i++) {
            labels[i]=new JLabel(labelnames[i]);
            labels[i].setVisible(true);
            ContentPanel.add(labels[i]);
            if (i==3) {
                passwd.setVisible(true);
                ContentPanel.add(passwd);
            } else if(i>3) {
                fields[i-1]=new JTextField(20);
                fields[i-1].setVisible(true);
                ContentPanel.add(fields[i-1]);
            } else {
                fields[i]=new JTextField(20);
                fields[i].setVisible(true);
                ContentPanel.add(fields[i]);
            }
        }
        JButton SubmitButton=new JButton("Zapisz się!");
        SubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkIfEmpty()==false) {
                    if (ValidationControl()==true) {
                        SaveData();
                    }
                    else {
                       JOptionPane.showMessageDialog(null, "Niepoprawne dane!");
                    }
                }
            }
        });
        SubmitButton.setVisible(true);
        ContentPanel.add(SubmitButton);
    }

    public boolean checkIfEmpty() {
        if (passwd.getPassword().length==0 || fields[0].getText().equals("") || fields[1].getText().equals("") || fields[2].getText().equals("") || fields[3].getText().equals("") || fields[4].getText().equals("")) {
            System.out.println("Proszę wypełnić wszystkie pola");
            JOptionPane.showMessageDialog(null, "Proszę wypełnić wszystkie pola oznaczone gwiazdką (*)");
            return true;
        }
        return false;
    }

    public void SaveData() {
            User user=new User(0, fields[0].getText(), fields[1].getText(), fields[2].getText(), new String(passwd.getPassword()), fields[3].getText(), fields[4].getText(), "Client");
            db.insertUser(user);
            JOptionPane.showMessageDialog(null, "Zostałeś zarejestrowany!");
            dispose();

    }

    public boolean ValidationControl() {
        ValidationController ControlFields=new ValidationController();
        for (int i=0; i<5; i++) {
            if (i==3) continue;
            ControlFields.SetStringAndValidationType(fields[i].getText(), ValidationType.BEGIN_FROM_LETTER);
            if (!ControlFields.Validate()) {
                return false;
            }
            if (i==4) {
                ControlFields.SetValidationType(ValidationType.INCLUDE_AT); //@//
                if (!ControlFields.Validate()) {
                    return false;
                }
                continue;
            }
            if (i==2) {
                continue;
            }
            ControlFields.SetValidationType(ValidationType.INCLUDE_ONLY_LETTERS);
            if (!ControlFields.Validate()) {
                return false;
            }

        }
        ControlFields.SetStringAndValidationType(fields[3].getText(), ValidationType.INCLUDE_NINE_DIGITS);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(new String(passwd.getPassword()), ValidationType.INCLUDE_AT_LEAST_EIGHT_CHARACTERS);
        if (!ControlFields.Validate()) {
            return false;
        }
        return true;
    }

    public String HashPassword(String passwordToHash) {
        String generatedPassword="";
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }

}
