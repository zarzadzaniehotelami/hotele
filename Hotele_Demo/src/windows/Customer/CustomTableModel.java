package windows.Customer;

import classes.HotelRoomJoin;
import classes.Room;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by pawelmrozikowski on 04.05.2017.
 */
public class CustomTableModel extends AbstractTableModel {
    private List<String> columnNames;
    private List<HotelRoomJoin> roomList;

    public CustomTableModel(List<String> columnNames, List<HotelRoomJoin> roomList)
    {
        this.columnNames = columnNames;
        this.roomList = roomList;
    }

    @Override
    public int getRowCount() {
        return roomList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        HotelRoomJoin room = roomList.get(rowIndex);
        switch(columnIndex)
        {
            case 0: return room.getRoomNumber();
            case 1: return room.getNumberOfPeople();
            case 2: return room.getPrice();
            case 3: return room.getAvailable();
            default: return null;
        }

    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        HotelRoomJoin room = roomList.get(rowIndex);
        switch(columnIndex)
        {
            case 0: room.setRoomNumber(Integer.parseInt(value.toString()));
            case 1: room.setNumberOfPeople(Integer.parseInt(value.toString()));
            case 2: room.setPrice(Integer.parseInt(value.toString()));
            case 3: room.setAvailable(Boolean.parseBoolean(value.toString()));
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public void update(List<HotelRoomJoin> roomList)
    {
        this.roomList = roomList;
        fireTableDataChanged();
    }

    public void addRow(HotelRoomJoin rowData)
    {
        roomList.add(rowData);
        fireTableDataChanged();
    }

    public void deleteRow(int rowNumber)
    {
        roomList.remove(rowNumber);
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }
}

