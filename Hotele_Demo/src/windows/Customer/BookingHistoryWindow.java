package windows.Customer;

import classes.HotelRoomBookingJoin;
import classes.User;
import database.Database;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class BookingHistoryWindow extends JFrame {

    private Database database;
    private BookingHistoryTableModel tableModel;
    private JTable historyTable;

    private List<String> columnNames;
    private List<HotelRoomBookingJoin> historyList;
    private List<HotelRoomBookingJoin> filteredHistoryList;
    private List<User> userList;


    private ClientAccountWindow clientAccountWindow;

    BookingHistoryWindow(ClientAccountWindow clientAccountWindow) {
        setBounds(100,100,400,400);
        setTitle("Bookings");
        //setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setLayout(new GridLayout(2, 1));
        setVisible(true);
        this.clientAccountWindow = clientAccountWindow;
        CreateComponents();
    }

    public void CreateComponents() {

        database = Database.getInstance();
        historyList = new LinkedList<>();
        historyList = database.innerJoinHotelRoomBooking();
        userList = new LinkedList<>();
        userList = database.selectUser();

        int idUser =  userList.stream().filter(u -> u.getLogin().equals(clientAccountWindow.getUsername())).map(u -> u.getIdUser()).findFirst().get();

        filteredHistoryList = historyList.stream()
                .filter(h -> h.getIdUser() == idUser).collect(Collectors.toList());

        JPanel mainPanel = new JPanel(new GridLayout(2,1));
        //GridBagConstraints gb = new GridBagConstraints();


        //mainPanel.setSize(new Dimension(200,385));
//        mainPanel.setBorder(new LineBorder(Color.BLACK));
        //mainPanel.setVisible(true);
        JPanel buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gb = new GridBagConstraints();

        JButton backButton = new JButton("Back");
        backButton.setPreferredSize(new Dimension(80, 30));
        gb.gridx = 0;
        gb.gridy = 0;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(0,10,0,0);
        gb.weightx = 1;
        gb.weighty = 1;
        buttonPanel.add(backButton, gb);
        backButton.addActionListener(e -> {
            clientAccountWindow.setVisible(true);
            setVisible(false);
        });

        JButton button = new JButton();
        button.setPreferredSize(new Dimension(80, 20));
        button.setVisible(false);
        gb.gridx = 0;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(0,10,0,0);
        gb.weightx = 1;
        gb.weighty = 1;
        buttonPanel.add(button, gb);
        mainPanel.add(buttonPanel);

        columnNames = Arrays.asList("HOTEL", "ADULTS", "PRICE", "CHECK IN", "CHECK OUT");
        tableModel = new BookingHistoryTableModel(columnNames, filteredHistoryList);
        historyTable = new JTable(tableModel);
        historyTable.setPreferredScrollableViewportSize(new Dimension(400, 100));
        JScrollPane scrollPane = new JScrollPane(historyTable);

        mainPanel.add(scrollPane);

        add(mainPanel);
    }
}
