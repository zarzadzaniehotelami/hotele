package windows.Customer;

/**
 * Created by Artur on 01.05.17.
 */
import classes.*;
import database.Database;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.List;

public class ConferenceRoomReserveWindow extends JFrame {

    private JButton backButton;
    private JLabel cityLabel;
    private JTextField cityTf;
    private JLabel dateLabel;
    private JXDatePicker datePicker;
    private JLabel confLabel;
    private JLabel chosenCityLabel;
    private JLabel hotelNameLabel;
    private JLabel capacityLabel;
    private JLabel chosenCapacityLabel;
    private JButton bookButton;
    private JButton searchButton;

    private Database database;
    private List<HotelConferenceRoomJoin> conferenceRoomJoinList;
    private List<User> userList;
    private List<Hotel> hotelList;
    private int idConferenceRoom;

    private ClientAccountWindow clientAccountWindow;


    public ConferenceRoomReserveWindow(ClientAccountWindow clientAccountWindow) {
        setTitle("Reserve conference room");
        setLayout(new FlowLayout());
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        CreateComponents();
        setVisible(true);
       // pack();
        this.clientAccountWindow = clientAccountWindow;
    }

    private void CreateComponents() {

        database = Database.getInstance();
        conferenceRoomJoinList = new LinkedList<>();
        conferenceRoomJoinList = database.innerJoinHotelConferenceRoom();
        userList = new LinkedList<>();
        userList = database.selectUser();
        hotelList = new LinkedList<>();
        hotelList = database.selectHotel();

        /*int idUser =  userList.stream()
                .filter(u -> u.getLogin().equals(clientAccountWindow.getUsername()))
                .map(u -> u.getIdUser()).findFirst().get();*/

        //System.out.println("user id "  + idUser);


        JPanel mainPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gb = new GridBagConstraints();

        backButton = new JButton("BACK");
        backButton.setPreferredSize(new Dimension(80,20));
        backButton.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 0;
        gb.gridy = 0;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(backButton, gb);
        backButton.addActionListener(e -> {
            clientAccountWindow.setVisible(true);
            this.setVisible(false);
        });

        cityLabel = new JLabel("Please choose the city");
        cityLabel.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 14));
        gb.gridx = 0;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(cityLabel, gb);

        cityTf = new JTextField(10);
        gb.gridx = 1;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(cityTf, gb);

        dateLabel = new JLabel("Date");
        dateLabel.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 14));
        gb.gridx = 0;
        gb.gridy = 2;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(dateLabel, gb);

        datePicker = new JXDatePicker();
        datePicker.setDate(Calendar.getInstance().getTime());
        datePicker.setFormats(new SimpleDateFormat("dd.MM.yyyy"));
        gb.gridx = 1;
        gb.gridy = 2;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(datePicker, gb);


        searchButton = new JButton("SEARCH");
        searchButton.setPreferredSize(new Dimension(80,20));
        searchButton.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 10));
        gb.gridx = 0;
        gb.gridy = 3;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(searchButton, gb);
        searchButton.addActionListener(e -> {
            idConferenceRoom = hotelList.stream()
                    .filter(h -> h.getCity().equals(cityTf.getText().trim()))
                    .map(h -> h.getIdConferenceRoom()).findFirst().get();
            for(HotelConferenceRoomJoin cr : conferenceRoomJoinList){
                if(cr.getCity().equals(cityTf.getText().trim())){
                    chosenCityLabel.setText(cr.getCity());
                    hotelNameLabel.setText(cr.getName());
                    capacityLabel.setText("CAPACITY: " + cr.getSize());
                }
            }
        });

        confLabel = new JLabel("CONFERENCE ROOM");
        confLabel.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 14));
        gb.gridx = 0;
        gb.gridy = 4;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(confLabel, gb);

        chosenCityLabel = new JLabel();
        chosenCityLabel.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 14));
        gb.gridx = 0;
        gb.gridy = 5;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(chosenCityLabel, gb);

        hotelNameLabel = new JLabel();
        hotelNameLabel.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 14));
        gb.gridx = 1;
        gb.gridy = 5;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(hotelNameLabel, gb);

        capacityLabel = new JLabel();
        capacityLabel.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 14));
        gb.gridx = 0;
        gb.gridy = 6;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(capacityLabel, gb);

        bookButton = new JButton("BOOK");
        bookButton.setPreferredSize(new Dimension(80,20));
        bookButton.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 0;
        gb.gridy =7;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(bookButton, gb);
        bookButton.addActionListener(e -> {
            LocalDate date = datePicker.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if(cityTf.getText().isEmpty()){
                JOptionPane.showMessageDialog(null, "Please enter a city");
            }
            else{
                int option = JOptionPane.showConfirmDialog(null, "Would you like to book that conference room",
                        "CONFERENCE ROOM", JOptionPane.YES_NO_OPTION);
                if(option == 0){
                    JOptionPane.showMessageDialog(null, "Conference room has been booked");
                    database.insertConferenceRoomBooking(new ConferenceRoomBooking(0, 1, idConferenceRoom, date));
                    setVisible(false);
                    clientAccountWindow.setVisible(true);
                }
            }
        });

        add(mainPanel);
    }
}
