package windows;

import database.Database;
import windows.Admin.AdminAccountWindow;
import windows.Customer.ClientAccountWindow;
import windows.Customer.ConferenceRoomReserveWindow;
import windows.Customer.RoomReserveWindow;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel{
    private Database db;
    private JButton welcomeButton;
    private LogginWindow loginWindow;
    private RoomReserveWindow roomReserveWindow;
    private ConferenceRoomReserveWindow conferenceRoomReserveWindow;
    private ClientAccountWindow clientAccountWindow;
    private AdminAccountWindow adminAccountWindow;

    //private UserPanel userPanel;

    // ta klasa jest glownym panelem, w ktorym beda sie laczyc wszystkiem pozostale panele (taki łącznik), bo muszą
    // się gdzieś ze sobą wszystkie okna łączyć, zeby nie było kolizji
    // no i tak sie zlozylo ze jest to tym samym okno powitalne( potem mozna je zmodyfikowac, poprawic wyglad itd)

    public MainPanel(Database _db){

        super(new CardLayout());
        db=_db;
        welcomeButton = new JButton("WITAMY W NASZYCH HOTELACH");
        welcomeButton.setPreferredSize(new Dimension(150,50));
        welcomeButton.addActionListener(e -> {
            CardLayout cardLayout = (CardLayout) (this.getLayout());
            cardLayout.show(this, "loginWindow");
            //cardLayout.show(this, "roomReserve");
            //cardLayout.show(this, "conferenceRoomReserve");
            //cardLayout.show(this, "clientAccount");
            //cardLayout.show(this, "admin");

        });
        add(welcomeButton);


        loginWindow = new LogginWindow(db);                   //dodawanie kolejnych okien
        add(loginWindow, "loginWindow");

        //roomReserveWindow = new RoomReserveWindow();
        //add(roomReserveWindow, "roomReserve");

        //conferenceRoomReserveWindow = new ConferenceRoomReserveWindow();
        //add(conferenceRoomReserveWindow, "conferenceRoomReserve");

        //clientAccountWindow = new ClientAccountWindow("a");
        //add(clientAccountWindow, "clientAccount");

        //adminAccountWindow = new AdminAccountWindow(db);
        //add(adminAccountWindow, "admin");

    }

}
