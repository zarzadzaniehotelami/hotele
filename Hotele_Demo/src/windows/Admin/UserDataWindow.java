package windows.Admin;

/**
 * Created by Artur on 03.05.17.
 */
import classes.User;
import database.Database;
import others.ValidationController;
import others.ValidationType;

import javax.swing.*;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserDataWindow extends JFrame {
    private boolean CanBeModified;
    private String username;
    private boolean ErrorDetected;
    private Database db;
    private JTextField TName, TSurname, TLogin, TPassword, TTel, TEmail;
    public UserDataWindow(boolean _CanBeModified, String _username, Database _db) {
        db=_db;
        ErrorDetected=false;
        username=_username;
        if (username=="Undefined User") ErrorDetected=true;
        CanBeModified=_CanBeModified;
        setSize(350, 250);
        setTitle("All Data: "+username);
        setLayout(new GridLayout(7, 1));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        CreateComponents();
        setVisible(true);
    }

    private void CreateComponents() {
        User user=db.selectUserByLogin(username);
        TName=new JTextField(20);
        TSurname=new JTextField(20);
        TLogin=new JTextField(20);
        TPassword=new JTextField(20);
        TTel=new JTextField(20);
        TEmail=new JTextField(20);
        add(new JLabel("imię:"));
        if (CanBeModified) {
            if (ErrorDetected) {
                TName.setText("Undefined");
            } else {
                TName.setText(user.getName());
            }
            add(TName);
        } else {
            if (ErrorDetected) {
                add(new JLabel("Undefined"));
            } else {
                add(new JLabel(user.getName()));
            }
        }
        add(new JLabel("nazwisko:"));
        if (CanBeModified) {
            if (ErrorDetected) {
                TSurname.setText("Undefined");
            } else {
                TSurname.setText(user.getSurname());
            }
            add(TSurname);
        } else {
            if (ErrorDetected) {
                add(new JLabel("Undefined"));
            } else {
                add(new JLabel(user.getSurname()));
            }
        }
        add(new JLabel("login:"));
        if (CanBeModified) {
            if (ErrorDetected) {
                TLogin.setText("Undefined");
            } else {
                TLogin.setText(user.getLogin());
            }
            add(TLogin);
        } else {
            if (ErrorDetected) {
                add(new JLabel("Undefined"));
            } else {
                add(new JLabel(user.getLogin()));
            }
        }
        add(new JLabel("hasło:"));
        if (CanBeModified) {
            if (ErrorDetected) {
                TPassword.setText("Undefined");
            } else {
                TPassword.setText(user.getPassword());
            }
            add(TPassword);
        } else {
            if (ErrorDetected) {
                add(new JLabel("Undefined"));
            } else {
                add(new JLabel(user.getPassword()));
            }
        }
        add(new JLabel("nr telefonu:"));
        if (CanBeModified) {
            if (ErrorDetected) {
                TTel.setText("Undefined");
            } else {
                TTel.setText(user.getPhoneNumber());
            }
            add(TTel);
        } else {
            if (ErrorDetected) {
                add(new JLabel("Undefined"));
            } else {
                add(new JLabel(user.getPhoneNumber()));
            }
        }
        add(new JLabel("adres email:"));
        if (CanBeModified) {
            if (ErrorDetected) {
                TEmail.setText("Undefined");
            } else {
                TEmail.setText(user.getEmailAddress());
            }
            add(TEmail);
        } else {
            if (ErrorDetected) {
                add(new JLabel("Undefined"));
            } else {
                add(new JLabel(user.getEmailAddress()));
            }
        }
        JButton btnEnter=new JButton("Save");
        btnEnter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ValidationControl()) {
                    User u=new User(user.getIdUser(), TName.getText(), TSurname.getText(), TLogin.getText(), TPassword.getText(), TTel.getText(), TEmail.getText(), user.getStatus());
                    db.updateUser(u);
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrect data in text fields.");
                }
            }
        });
        if (!CanBeModified) btnEnter.setEnabled(false);
        add(btnEnter);
        JButton btnOk=new JButton("Ok");
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(btnOk);
    }

    public boolean ValidationControl() {
        ValidationController ControlFields=new ValidationController();
        ControlFields.SetStringAndValidationType(TName.getText(), ValidationType.BEGIN_FROM_LETTER);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetString(TSurname.getText());
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetString(TLogin.getText());
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetString(TEmail.getText());
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TName.getText(), ValidationType.INCLUDE_ONLY_LETTERS);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetString(TSurname.getText());
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TTel.getText(), ValidationType.INCLUDE_NINE_DIGITS);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TPassword.getText(), ValidationType.INCLUDE_AT_LEAST_EIGHT_CHARACTERS);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TEmail.getText(), ValidationType.INCLUDE_AT);
        if (!ControlFields.Validate()) {
            return false;
        }
        return true;
    }

}
