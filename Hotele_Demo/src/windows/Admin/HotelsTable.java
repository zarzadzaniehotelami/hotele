package windows.Admin;

import classes.Hotel;
import database.Database;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class HotelsTable  extends AbstractTableModel{
    private Database db;
    private List<Hotel> values;
    private String []columnNames;
    public HotelsTable(Database _db) {
        db=_db;
        LoadData();
        columnNames=new String[9];
        columnNames[0]="id";
        columnNames[1]="name";
        columnNames[2]="city";
        columnNames[3]="number of rooms";
        columnNames[4]="relaxation room";
        columnNames[5]="menu meals";
        columnNames[6]="menu drinks";
        columnNames[7]="is parking";
        columnNames[8]="id conference room";
    }
    public void LoadData() {
        values=db.selectHotel();
    }

    @Override
    public int getRowCount() {
        return values.size();
    }

    public Hotel getHotelFromRow(int row) {
        return new Hotel((int)getValueAt(row, 0), (String)getValueAt(row, 1), (String)getValueAt(row, 2), (int)getValueAt(row, 3), (String)getValueAt(row, 4), (String)getValueAt(row, 5), (String)getValueAt(row, 6), (boolean)getValueAt(row, 7), (int)getValueAt(row, 8));
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (column==0)
            return values.get(row).getIdHotel();
        if (column==1)
            return values.get(row).getName();
        if (column==2)
            return values.get(row).getCity();
        if (column==3)
            return values.get(row).getNumberOfRooms();
        if (column==4)
            return values.get(row).getRelaxationRoom();
        if (column==5)
            return values.get(row).getMenuMeals();
        if (column==6)
            return values.get(row).getMenuDrinks();
        if (column==7) {
            return values.get(row).getIsParking();
        }
        if (column==8)
            return values.get(row).getIdConferenceRoom();
        return null;
    }

}
