package windows.Admin;

import database.Database;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HotelsManageWindow extends JFrame {
    private Database db;
    private HotelsTable hTable;
    private JTable table;
    private int selected;
    public HotelsManageWindow(Database _db) {
        selected=-1;
        db=_db;
        setTitle("Zarządzanie hotelami");
        setSize(600, 300);
        setLayout(new FlowLayout());
        CreateComponents();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
    private void CreateComponents() {
        JPanel Controller=new JPanel();
        Controller.setLayout(new FlowLayout());
        Controller.setSize(100, 200);
        JButton btnAddHotel=new JButton("Dodaj Hotel");
        JButton btnDeleteHotel=new JButton("Usuń Hotel");
        JButton btnModifyHotel=new JButton("Modyfikuj Hotel");
        JButton btnRefresh=new JButton("Odśwież");
        btnAddHotel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new HotelsFormWindow(db);
            }
        });
        btnDeleteHotel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected=table.getSelectedRow();
                if (selected!=-1) {
                    db.deleteHotel(hTable.getHotelFromRow(selected).getIdHotel());
                    hTable.LoadData();
                    table.repaint();
                    selected=-1;
                } else {
                    JOptionPane.showMessageDialog(null, "Choose record first");
                }
            }
        });
        btnModifyHotel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected=table.getSelectedRow();
                if (selected!=-1) {
                    new HotelsDataWindow(true, hTable.getHotelFromRow(selected).getIdHotel(), db);
                    table.repaint();
                    selected=-1;
                } else {
                    JOptionPane.showMessageDialog(null, "Choose record first");
                }
            }
        });
        btnRefresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hTable.LoadData();
                table.repaint();
            }
        });
        Controller.add(btnAddHotel);
        Controller.add(btnDeleteHotel);
        Controller.add(btnModifyHotel);
        Controller.add(btnRefresh);
        add(Controller);
        hTable=new HotelsTable(db);
        table=new JTable(hTable);
        table.setPreferredSize(new Dimension(200, 400));
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrollpane=new JScrollPane(table);
        scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scrollpane);
    }
}
