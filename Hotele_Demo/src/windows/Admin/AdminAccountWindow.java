package windows.Admin;

import database.Database;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Artur on 16.05.17.
 */
public class AdminAccountWindow extends JFrame {
    private Database db;
    private JButton btnUsersManage;
    private JButton btnHotelsManage;
    private JButton btnRoomsManage;
    private JButton btnConferenceRoomManage;
    private JButton btnBookedRooms;
    private JButton btnBookedConferenceRooms;
    private JButton btnMenu;
    private JButton btnLogout;
    public AdminAccountWindow(Database _db) {
        db=_db;
        setTitle("Admin");
        setSize(300, 600);
        setLayout(new GridLayout(8, 1));
        //setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        CreateComponents();
        setVisible(true);
    }
    private void CreateComponents() {
        btnUsersManage=new JButton("Zarządzanie użytkownikami");
        btnUsersManage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsersManageWindow(db);
            }
        });
        add(btnUsersManage);
        btnHotelsManage=new JButton("Zarządzanie hotelami");
        btnHotelsManage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new HotelsManageWindow(db);

            }
        });
        add(btnHotelsManage);
        btnRoomsManage=new JButton("Zarządzanie pokojami");
        btnRoomsManage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        add(btnRoomsManage);
        btnConferenceRoomManage=new JButton("Zarządzanie salami konferencyjnymi");
        btnConferenceRoomManage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        add(btnConferenceRoomManage);
        btnBookedRooms=new JButton("Zarządzanie rezerwacjami pokojów");
        btnBookedRooms.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        add(btnBookedRooms);
        btnBookedConferenceRooms=new JButton("Zarządzanie rezerwacjami sal konferencyjnych");
        btnBookedConferenceRooms.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        add(btnBookedConferenceRooms);
        btnMenu=new JButton("Zarządzanie menu");
        btnMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        add(btnMenu);
        btnLogout=new JButton("Wyloguj się");
        btnLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(btnLogout);
    }
}
