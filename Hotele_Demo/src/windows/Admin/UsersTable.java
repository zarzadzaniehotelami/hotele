package windows.Admin;

import classes.User;
import database.Database;

    import javax.swing.table.AbstractTableModel;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Artur on 17.05.17.
 */
public class UsersTable extends AbstractTableModel {
    private Database db;
    private List<User> values;
    private String []columnNames;
    public UsersTable(Database _db) {
        db=_db;
        LoadData();
        columnNames=new String[8];
        columnNames[0]="id";
        columnNames[1]="name";
        columnNames[2]="surname";
        columnNames[3]="login";
        columnNames[4]="password";
        columnNames[5]="email address";
        columnNames[6]="phone number";
        columnNames[7]="status";
    }
    public void LoadData() {
        values=db.selectUser();
    }

    @Override
    public int getRowCount() {
        return values.size();
    }

    public User GetUserFromRow(int row) {
        return new User((int)getValueAt(row, 0), (String)getValueAt(row, 1), (String)getValueAt(row, 2), (String)getValueAt(row, 3), (String)getValueAt(row, 4), (String)getValueAt(row, 5), (String)getValueAt(row, 6), (String)getValueAt(row, 7));
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (column==0)
            return values.get(row).getIdUser();
        if (column==1)
            return values.get(row).getName();
        if (column==2)
            return values.get(row).getSurname();
        if (column==3)
            return values.get(row).getLogin();
        if (column==4)
            return values.get(row).getPassword();
        if (column==5)
            return values.get(row).getEmailAddress();
        if (column==6)
            return values.get(row).getPhoneNumber();
        if (column==7)
            return values.get(row).getStatus();
        return null;
    }


}
