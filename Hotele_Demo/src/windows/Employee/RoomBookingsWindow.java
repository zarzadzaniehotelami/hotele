package windows.Employee;

import classes.Hotel;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import database.Database;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.lowagie.text.*;

public class RoomBookingsWindow extends JFrame {
    private Database db;
    private Hotel hotel;
    private int selected = -1;
    private JPanel MainPanel;
    private JTable table;

    public RoomBookingsWindow(Hotel hotel, Database db){
        this.hotel = hotel;
        this.db = db;
        setLocation(300,300);
        setSize(600,300);
        setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        CreateComponents();
        setVisible(true);
    }

    public void CreateComponents(){
        MainPanel = new JPanel();
        MainPanel.setLayout(new FlowLayout());
        MainPanel.setSize(100,200);

        RoomBookingsTable bookings = new RoomBookingsTable(hotel.getIdHotel(), db);
        table = new JTable(bookings);
        table.setPreferredSize(new Dimension(200,400));
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        JButton deleteButton = new JButton("Usuń rezerwację");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected = table.getSelectedRow();
                if(selected > -1) {
                    db.deleteRoomBooking(bookings.getRoomBookingIdFromRow(selected));
                    bookings.getBookings();
                    table.repaint();
                    selected = -1;
                }
                else
                    JOptionPane.showMessageDialog(null, "Choose booking");
            }
        });

        JButton raportButton = new JButton("Wyślij raport");
        raportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = Calendar.getInstance().getTime();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = dateFormat.format(date);
                String name = "raport-" + formattedDate + "-pokojeRezerwacja.pdf";
                print(name);
            }
        });

        add(MainPanel);
        MainPanel.add(deleteButton);
        MainPanel.add(raportButton);
        add(scroll);
        MainPanel.setVisible(true);
    }

    private void print(String name) {
        try {
            int count = table.getRowCount();
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(name));
            document.open();
            PdfPTable tab = new PdfPTable(7);
            tab.addCell("ID");
            tab.addCell("Rezerwujący");
            tab.addCell("Numer pokoju");
            tab.addCell("Ilość gości");
            tab.addCell("Cena");
            tab.addCell("Data przybycia");
            tab.addCell("Data wyjazdu");

            for (int i = 0; i < count; i++) {
                Object[] obj = new Object[7];
                String[] value = new String[7];
                for (int j = 0; j < 7; j++) {
                    obj[j] = table.getModel().getValueAt(i, j);
                    value[j] = obj[j].toString();
                    tab.addCell(value[j]);
                }
            }
            document.add(tab);
            document.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Nie udało się wysłać do pdf");
        }
    }
}
