package windows.Employee;

import database.Database;
import classes.Room;
import others.ValidationController;
import others.ValidationType;
import sun.applet.Main;

import javax.naming.ldap.Control;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddRoomWindow extends JFrame {
    private Database db;
    private int hotelID;
    private JTextField fields[];
    private JPanel MainPanel;

    AddRoomWindow(int id, Database db) {
        this.db = db;
        hotelID = id;
        setBounds(100,100,400,800);
        setTitle("Dodaj pokój");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new FlowLayout());
        setVisible(true);
        MainPanel = new JPanel();
        MainPanel.setLayout(new GridLayout(4,3));
        setBounds(110,110,500,300);
        MainPanel.setBorder(new LineBorder(Color.black));
        MainPanel.setVisible(true);
        add(MainPanel);
        CreateComponents();
        pack();
    }

    public void CreateComponents() {
        JLabel labels[] = new JLabel[3];
        fields = new JTextField[3];
        labels[0] = new JLabel("Numer pokoju");
        labels[0].setVisible(true);
        MainPanel.add(labels[0]);
        fields[0] = new JTextField(20);
        fields[0].setVisible(true);
        MainPanel.add(fields[0]);
        labels[1] = new JLabel("Ilość gości");
        labels[1].setVisible(true);
        MainPanel.add(labels[1]);
        fields[1] = new JTextField(20);
        fields[1].setVisible(true);
        MainPanel.add(fields[1]);
        labels[2] = new JLabel("Cena");
        labels[2].setVisible(true);
        MainPanel.add(labels[2]);
        fields[2] = new JTextField(20);
        fields[2].setVisible(true);
        MainPanel.add(fields[2]);

        JButton addButton = new JButton("Dodaj");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ValidationControl())
                    addRoom();
                else
                    JOptionPane.showMessageDialog(null,"Niepoprawne dane!");
            }
        });

        addButton.setVisible(true);
        MainPanel.add(addButton);
    }

    public boolean ValidationControl() {

        for(int i = 0; i < 3; i++) {
            try {
                Integer.parseInt(fields[i].getText());
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Niepoprawne dane");
                return false;
            }
        }
        return true;
    }

    public void addRoom() {
        Room room = new Room(0,hotelID,Integer.parseInt(fields[0].getText()),Integer.parseInt(fields[1].getText()), Integer.parseInt(fields[2].getText()),true);
        db.insertRoom(room);
        JOptionPane.showMessageDialog(null, "Dodano pokój");
        dispose();
    }

}
