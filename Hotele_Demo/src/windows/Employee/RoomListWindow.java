package windows.Employee;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import database.Database;
import classes.Hotel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;

public class RoomListWindow extends JFrame {

    private Database db;
    private Hotel hotel;
    private int selected = -1;
    private JPanel MainPanel;
    private JTable table;

    public RoomListWindow(Hotel hotel, Database db) {

        this.db = db;
        this.hotel = hotel;
        setLocation(300,300);
        setSize(600,300);
        setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        setResizable(false);
        CreateComponents();
        setVisible(true);
    }

    private void CreateComponents() {
        MainPanel = new JPanel();
        MainPanel.setLayout(new FlowLayout());
        MainPanel.setSize(100,200);

        RoomTable rt = new RoomTable(hotel.getIdHotel(), db);
        table = new JTable(rt);
        table.setPreferredSize(new Dimension(200,400));
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

//        JButton modifyRoom = new JButton("Modyfikuj pokój");

        JButton deleteRoom = new JButton("Usuń pokój");
        deleteRoom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected=table.getSelectedRow();

                if(selected > -1) {
                    db.deleteRoom(rt.getRoomIdFromRow(selected));
                    rt.getRooms();
                    table.repaint();
                    selected = -1;
                }
                else {
                    JOptionPane.showMessageDialog(null, "Choose room");
                }

            }
        });
        JButton addRoom = new JButton("Dodaj pokój");
        addRoom.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new AddRoomWindow(hotel.getIdHotel(), db);
                }
            });

        JButton refresh = new JButton("Odśwież");
        refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               rt.getRooms();
               table.repaint();

            }
        });
//        JButton raport = new JButton("raport");
//        raport.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                print();
//            }
//        });

//        MainPanel.add(modifyRoom);
        MainPanel.add(deleteRoom);
        MainPanel.add(addRoom);
        MainPanel.add(refresh);
//        MainPanel.add(raport);
        add(MainPanel);

        add(scroll);
        MainPanel.setVisible(true);


    }

        private void print() {
            try

            {
                int count = table.getRowCount();
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream("../../../Pulpit/pokoje.pdf"));
                document.open();
                PdfPTable tab = new PdfPTable(5);
                tab.addCell("A");
                tab.addCell("B");
                tab.addCell("C");
                tab.addCell("D");
                tab.addCell("E");

                for (int i = 0; i < count; i++) {
                    Object[] obj = new Object[5];
                    String[] value = new String[5];
                    for (int j = 0; j < 5; j++) {
                        obj[j] = table.getModel().getValueAt(i, j);
                        value[j] = obj[j].toString();
                        tab.addCell(value[j]);
                    }
                }
                document.add(tab);
                document.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Nie udało się wysłać do pdf");
            }
        }

}
