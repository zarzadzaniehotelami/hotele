package windows.Employee;

import database.Database;
import classes.Hotel;
import javax.swing.*;
import java.awt.*;

public class HotelInfoWindow extends JFrame {

    private Database db;
    private Hotel hotel;
    private JPanel MainPanel;

    public HotelInfoWindow(Hotel hotel, Database db) {
        this.db = db;
        this.hotel = hotel;
        setLocation(300,300);
        setSize(400,400);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        CreateComponents();
        setVisible(true);
    }

    private void CreateComponents() {
        MainPanel = new JPanel();
//        MainPanel.setSize(320,200);
        MainPanel.setBounds(310,310,380,400);
        MainPanel.setLayout(new GridLayout(7, 2));
        MainPanel.setVisible(true);
        add(MainPanel);

        JLabel label1 = new JLabel();
        JLabel label1a = new JLabel();
        label1.setText("<html><h1>Info o hotelu</h1></html>");
        label1a.setText(this.hotel.getName());
        label1a.setFont(new Font("Myriad Pro",Font.PLAIN,15));
        MainPanel.add(label1);
        MainPanel.add(label1a);

        JLabel label2 = new JLabel();
        JLabel label2a = new JLabel();
        label2.setText("miasto");
        label2a.setText(this.hotel.getCity());
        label2.setFont(new Font("Arial",Font.PLAIN,11));

        JLabel label3 = new JLabel();
        JLabel label3a= new JLabel();
        label3.setText("ilość pokoi");
        label3a.setText(String.format("" + this.hotel.getNumberOfRooms()));
        label3.setFont(new Font("Arial", Font.PLAIN, 11));
        label3a.setFont(new Font("Arial", Font.PLAIN, 11));

        JLabel label4 = new JLabel();
        JLabel label4a = new JLabel();
        label4.setText("atrakcje");
        label4a.setText(this.hotel.getRelaxationRoom());
        label4.setFont(new Font("Arial", Font.PLAIN, 11));
        label4a.setFont(new Font("Arial", Font.PLAIN, 11));

        JLabel label5 = new JLabel();
        JLabel label5a = new JLabel();
        label5.setText("menu dania");
        label5a.setText(this.hotel.getMenuMeals());
        label5.setFont(new Font("Arial", Font.PLAIN, 11));
        label5a.setFont(new Font("Arial", Font.PLAIN, 11));

        JLabel label6 = new JLabel();
        JLabel label6a = new JLabel();
        label6.setText("menu napoje");
        label6a.setText(this.hotel.getMenuDrinks());
        label6.setFont(new Font("Arial", Font.PLAIN, 11));
        label6a.setFont(new Font("Arial", Font.PLAIN, 11));

        JLabel label7 = new JLabel();
        JLabel label7a = new JLabel();
        label7.setText("parking");
        label7a.setText(String.format("" + this.hotel.getIsParking()));
        label7.setFont(new Font("Arial", Font.PLAIN, 11));
        label7a.setFont(new Font("Arial", Font.PLAIN, 11));

        JLabel label8 = new JLabel();
        JLabel label8a = new JLabel();
        label8.setText("sale konferencyjne");
        label8a.setText(String.format("" + this.hotel.getIdConferenceRoom()));
        label8.setFont(new Font("Arial", Font.PLAIN, 11));
        label8a.setFont(new Font("Arial", Font.PLAIN, 11));

        MainPanel.add(label2);
        MainPanel.add(label2a);
        MainPanel.add(label3);
        MainPanel.add(label3a);
        MainPanel.add(label4);
        MainPanel.add(label4a);
        MainPanel.add(label5);
        MainPanel.add(label5a);
        MainPanel.add(label6);
        MainPanel.add(label6a);
        MainPanel.add(label7);
        MainPanel.add(label7a);
        MainPanel.add(label8);
        MainPanel.add(label8a);
    }
}
