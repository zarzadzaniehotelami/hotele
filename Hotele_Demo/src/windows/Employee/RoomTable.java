package windows.Employee;

import database.Database;
import classes.Room;
import javax.swing.table.AbstractTableModel;
import java.util.*;

public class RoomTable extends AbstractTableModel {

    private Database db;
    private List<Room> rooms;
    private int hotelID;
    private String[] names;

    public RoomTable(int hotelID, Database db) {
        this.db = db;
        this.hotelID = hotelID;
        getRooms();
        names = new String[6];
        names[0]="id";
        names[1]="nazwa hotelu";
        names[2]="numer na drzwiach";
        names[3]="ilość gości";
        names[4]="cena";
        names[5]="czy dostępny";
    }

    public void getRooms() {
        rooms = db.selectRoomByHotelId(hotelID);
    }

    public int getRowCount() {
        return rooms.size();
    }

    public Object getValueAt(int row, int column) {

        switch(column) {
            case 0: return rooms.get(row).getIdRoom();
            case 1: return db.selectHotelById(hotelID).getName();
            case 2: return rooms.get(row).getRoomNumber();
            case 3: return rooms.get(row).getNumberOfPeople();
            case 4: return rooms.get(row).getPrice();
            case 5: return rooms.get(row).getIfAvailable();
            default: return null;
        }
    }

    public String getColumnName(int ix) {
        return names[ix];
    }

    public int getColumnCount() {
        return 6;
    }

    public Room getRoomFromRow(int row) {
        return new Room((int) getValueAt(row,0), hotelID, (int) getValueAt(row,3), (int)  getValueAt(row,4), (int) getValueAt(row,5), (boolean) getValueAt(row,6));
    }

    public int getRoomIdFromRow(int row) {
        return (int) getValueAt(row,0);
    }
}

