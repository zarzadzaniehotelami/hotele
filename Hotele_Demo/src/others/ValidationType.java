package others;

/**
 * Created by Artur on 11.05.17.
 */
public enum ValidationType {
    INCLUDE_ONLY_DIGITS (1),
    INCLUDE_ONLY_LETTERS (2),
    INCLUDE_AT (3),
    INCLUDE_NINE_DIGITS (4),
    INCLUDE_AT_LEAST_EIGHT_CHARACTERS (5),
    INCLUDE_ONLY_LETTERS_AND_WHITESPACES (6),
    BEGIN_FROM_LETTER (6);

    private final int type;

    ValidationType(int type) {
        this.type=type;
    }

    public int getTypeCode() {
        return this.type;
    }
}
