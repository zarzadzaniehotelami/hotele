package others;

/**
 * Created by Artur on 11.05.17.
 */
public class ValidationController {
    private String str;
    private ValidationType type;

    public void SetString(String _str) {
        str=_str;
    }

    public String GetString() {
        return str;
    }

    public void SetValidationType(ValidationType _type) {
        type=_type;
    }

    public ValidationType GetValidationType() {
        return type;
    }

    public void SetStringAndValidationType(String _str, ValidationType _type) {
        str=_str;
        type=_type;
    }

    public boolean Validate() {
        boolean ret=true;
        switch(type) {
            case INCLUDE_ONLY_DIGITS : {
                /*for (int i=0; i<str.length(); i++) {
                    Character c=str.charAt(i);
                    if (!Character.isDigit(c)) {
                        ret=false;
                    }
                }*/
                char []chars=str.toCharArray();
                for (char c : chars) {
                    if (!Character.isLetter(c)) {
                        ret=false;
                    }
                }
            }
            break;
            case INCLUDE_ONLY_LETTERS : {
                /*for (int i=0; i<str.length(); i++) {
                    Character c=str.charAt(i);
                    if (!Character.isLetter(c)) {
                        ret=false;
                    }
                }*/
                char []chars=str.toCharArray();
                for (char c : chars) {
                    if (!Character.isLetter(c)) {
                        ret=false;
                    }
                }
            }
            break;
            case INCLUDE_AT : {
                boolean found=false;
                for (int i=0; i<str.length(); i++) {
                    Character c=str.charAt(i);
                    if (c=='@') {
                        found=true;
                        break;
                    }
                }
                if (!found) ret=false;
            }
            break;
            case INCLUDE_NINE_DIGITS : {
                int k=0;
                for (int i=0; i<str.length(); i++) {
                    Character c=str.charAt(i);
                    if (Character.isDigit(c)) {
                        k++;
                    }
                }
                if (k!=9) ret=false;
            }
            break;
            case INCLUDE_AT_LEAST_EIGHT_CHARACTERS : {
                if (str.length()<8) ret=false;
            }
            break;
            case BEGIN_FROM_LETTER : {
                Character c=str.charAt(0);
                if (!Character.isLetter(c)) ret=false;
            }
            break;
            default : ret=false;
            break;
        }
        return ret;
    }
}
