import classes.Hotel;
import classes.Room;
import classes.*;
import database.Database;
import windows.MainPanel;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class Main  extends JFrame {

    public static void createAndShowGui(Database db)
    {
        JFrame frame = new JFrame("HOTELS");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        MainPanel panel = new MainPanel(db); //glowny panel, ktory bedzie laczyl w sobie wszystkie pozostale okna, przelaczal
                                           //w zaleznosci od potrzeby
        panel.setVisible(true);
        frame.setContentPane(panel); //zawiera ten panel glowny

        frame.pack(); //ustawia rozmiar w zaleznosci od komponentow
        frame.setVisible(true); //zeby bylo widoczne
        frame.setResizable(true); //rozciaganie okna 
    }
    public static void main(String[] args) {

        Database database = Database.getInstance();



       /* database.insertUser(new User(0,"Admin", "Admin", "Root", "Root", null, null, null));
        database.insertUser(new User(0,"Kasia", "Nowak", "Employee", "Employee",
                "163556789", "nowakas@gmail.com", "Employee"));
        database.insertUser(new User(0, "Adam", "Pawłowicz", "Manager", "Manager", "765435234", "pawlowadam@gmail.com", "manager"));

        database.insertUser(new User(0,"Adam", "Kowalski", "kowal", "1234",
                "123456789", "kow@gmail.com", "Client"));
        database.insertUser(new User(0,"Piotr", "Nowak", "nowak", "nowak1",
                "123458789", "nowak@gmail.com", "Client"));
        database.insertUser(new User(0,"Magdalena", "Kosakowska", "kosak", "kosak123",
                "145785789", "kosak.m@gmail.com", "Client"));
        database.insertUser(new User(0,"Kamil", "Stoch", "kamil", "stoch",
                "123416289", "nowak@gmail.com", "Client"));
        database.insertUser(new User(0,"Anna", "Kozak", "kozal", "kozak1",
                "123619789", "kozak@gmail.com", "Client"));


        database.insertConferenceRoom(new ConferenceRoom(0, 50));
        database.insertConferenceRoom(new ConferenceRoom(0, 100));
        database.insertConferenceRoom(new ConferenceRoom(0, 200));


        database.insertHotel(new Hotel(0, "Royal Krakow", "Krakow", 200, "sport, kregle, spa",
                "Danie Polskie, Danie Wloskie, Danie Francuskie",
                "herbata, kawa, soki owocowe",
                true, 1));

        database.insertHotel(new Hotel(0, "Royal Poznan", "Poznan", 200, "kregle, spa",
                "Danie Polskie, Danie Wloskie, Danie Azjatyckie",
                "kawa, herbata, soki owocowe", true, 2));
        database.insertHotel(new Hotel(0, "Royal Warsaw", "Warszawa", 500, "kregle, spa, basen,",
                "Danie Polskie, Danie Wloskie, Danie Azjatyckie, Danie Francuskie",
                "kawa, herbata, soki owocowe", true, 3));


        database.insertRoom(new Room(0, 1, 250, 3, 300, true));
        database.insertRoom(new Room(0, 1, 251, 2, 250, true));
        database.insertRoom(new Room(0, 1, 252, 1, 170, true));
        database.insertRoom(new Room(0, 2, 101, 2, 310, true));
        database.insertRoom(new Room(0, 2, 102, 3, 280, true));
        database.insertRoom(new Room(0, 2, 103, 1, 120, true));
        database.insertRoom(new Room(0, 3, 16, 2, 270, true));
        database.insertRoom(new Room(0, 3, 17, 3, 330, true));
        database.insertRoom(new Room(0, 3, 18, 1, 180, true));

        database.insertRoomBooking(new RoomBooking(0, 4, 1, 3,
                300, LocalDate.of(2017, 06, 12), LocalDate.of(2017, 06, 13)));
        database.insertRoomBooking(new RoomBooking(0, 5, 2, 2,
                250, LocalDate.of(2017, 06, 16), LocalDate.of(2017, 06, 17)));
        database.insertRoomBooking(new RoomBooking(0, 6, 2, 1,
                300, LocalDate.of(2017, 06, 18), LocalDate.of(2017, 06, 19)));

        database.insertConferenceRoomBooking(new ConferenceRoomBooking(0, 4, 1, LocalDate.of(2017, 06, 18)));
        database.insertConferenceRoomBooking(new ConferenceRoomBooking(0, 4, 2, LocalDate.of(2017, 06, 20)));
        database.insertConferenceRoomBooking(new ConferenceRoomBooking(0, 6, 3, LocalDate.of(2017, 06, 21)));*/


        //new ClientAccountWindow(null);

       /* database.deleteUser(10);
        for (int i=0; i<database.selectUser().size(); i++) {
            System.out.println(database.selectUser().get(i));
        }*/
        /*System.out.println(database.selectUserByLogin("kowal").getPhoneNumber());
        System.out.println(database.selectHotelById(1).getName());*/


       //System.out.println(database.selectUserByLogin("Employee"));


        //System.out.println(database.getUserStatus("Employee"));

        Date date= Calendar.getInstance().getTime();
        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate=dateFormat.format(date);
        String name = "raport-" + formattedDate + ".pdf";
        System.out.println(name);


        javax.swing.SwingUtilities.invokeLater(
                new Runnable()
                {
                    public void run()
                    {
                        createAndShowGui(database);
                    }
                }
        );
    }
}
