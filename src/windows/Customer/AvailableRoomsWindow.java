package windows.Customer;

import classes.HotelRoomBookingJoin;
import classes.HotelRoomJoin;
import classes.RoomBooking;
import classes.User;
import database.Database;
import org.jdesktop.swingx.JXLabel;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.util.*;
import java.util.List;

public class AvailableRoomsWindow extends JPanel {

    private JButton back;
    private JLabel hotelName1;
    private JLabel hotelName2;
    private JLabel city;
    private JLabel relaxationRoom1;
    private JLabel relaxationRoom2;
    private JLabel menu;
    private JLabel menuMeals;
    private JLabel menuDrinks;
    private JLabel isParking1;
    private JLabel isParking2;
    private JTable roomsTable;
    private CustomTableModel customTableModel;
    private JButton bookRoomButon;

    private List<HotelRoomJoin> hotelRoomJoinList;
    private List<String> columnNames;
    private List<HotelRoomJoin> choosenRooms;
    private List<User> userList;
    private List<HotelRoomBookingJoin> roomsHistory;

    private Database database;

    private RoomReserveWindow roomReserveWindow;

    public AvailableRoomsWindow(RoomReserveWindow roomReserveWindow) {

        super(new GridBagLayout());

        this.roomReserveWindow = roomReserveWindow;

        database = Database.getInstance();
        hotelRoomJoinList = new LinkedList<>();
        hotelRoomJoinList = database.innerJoinHotelRoom();

        userList = new LinkedList<>();
        userList = database.selectUser();


        GridBagConstraints gb1 = new GridBagConstraints();


        JPanel mainPanel = new JPanel(new GridBagLayout());

        GridBagConstraints gb = new GridBagConstraints();

        back = new JButton("<");
        back.setPreferredSize(new Dimension(80,20));
        back.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 16));
        gb.gridx = 0;
        gb.gridy = 0;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(back, gb);
        back.addActionListener(e ->{
            this.setVisible(false);
            roomReserveWindow.setVisible(true);
        });

        hotelName1 = new JLabel("HOTEL");
        hotelName1.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 0;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(hotelName1, gb);

        hotelName2 = new JLabel();
        hotelName2.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 1;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(hotelName2, gb);

        city = new JLabel();
        city.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 2;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(city, gb);

        relaxationRoom1 = new JLabel("Relaxation");
        relaxationRoom1.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 0;
        gb.gridy = 2;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(relaxationRoom1, gb);

        relaxationRoom2 = new JXLabel();
        relaxationRoom2.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 1;
        gb.gridy = 2;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(relaxationRoom2, gb);

        menu = new JLabel("MENU");
        menu.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 0;
        gb.gridy = 3;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(menu, gb);

        menuMeals = new JLabel();
        menuMeals.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 1;
        gb.gridy = 3;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(menuMeals, gb);

        menuDrinks = new JLabel();
        menuDrinks.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 2;
        gb.gridy = 3;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(menuDrinks, gb);

        isParking1 = new JLabel("Parking");
        isParking1.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb.gridx = 0;
        gb.gridy = 4;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(isParking1, gb);

        isParking2 = new JLabel();
        gb.gridx = 1;
        gb.gridy = 4;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(isParking2, gb);

        gb1.gridx = 0;
        gb1.gridy = 0;
        gb1.anchor = GridBagConstraints.LINE_START;
        gb1.insets = new Insets(5,10,5,0);
        gb1.weightx = 1;
        gb1.weighty = 1;
        add(mainPanel, gb1);


        choosenRooms = new LinkedList<>();

        columnNames = Arrays.asList("NUMBER", "ADULTS", "PRICE", "IS AVAILABLE");
        customTableModel = new CustomTableModel(columnNames, hotelRoomJoinList);
        roomsTable = new JTable(customTableModel);
        roomsTable.setPreferredScrollableViewportSize(new Dimension(400, 100));
        roomsTable.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        JScrollPane scrollPane = new JScrollPane(roomsTable);
        gb1.gridx = 0;
        gb1.gridy = 1;
        gb1.anchor = GridBagConstraints.LINE_START;
        gb1.insets = new Insets(5,10,5,0);
        gb1.weightx = 2;
        gb1.weighty = 1;
        add(scrollPane, gb1);

        roomsHistory = new LinkedList<>();

        bookRoomButon = new JButton("BOOK");
        gb1.gridx = 0;
        gb1.gridy = 2;
        gb1.anchor = GridBagConstraints.LINE_START;
        gb1.insets = new Insets(5,10,5,0);
        gb1.weightx = 2;
        gb1.weighty = 1;
        add(bookRoomButon, gb1);
        bookRoomButon.addActionListener(e -> {
            if(roomsTable.getSelectedRow() == -1){
                JOptionPane.showMessageDialog(null, "Prosze wybrac pokoj");
            }
            else {
                int price = roomReserveWindow.getNumberOfDays() * roomReserveWindow.getNumberOfPeople() * (int) customTableModel.getValueAt(roomsTable.getSelectedRow(), 2);
                int option = JOptionPane.showConfirmDialog(null,
                        "Chcesz zarezerwowac pokoj w " + city.getText() + " na " + roomReserveWindow.getNumberOfDays() + " dni: cena " + price,
                        "ROOM TO BOOK", JOptionPane.YES_NO_OPTION);

                LocalDate arrivaleDate = roomReserveWindow.getArrivaleDate();
                LocalDate departureDate = roomReserveWindow.getDepartureDate();
                int idUser = userList.stream().filter(u -> u.getLogin().equals(roomReserveWindow.getUserName())).map(u -> u.getIdUser()).findFirst().get();
                int idRoom = (int) customTableModel.getValueAt(roomsTable.getSelectedRow(), 2);
                int numberOfPeople = roomReserveWindow.getNumberOfPeople();
                if (option == 0) {
                    JOptionPane.showMessageDialog(null, "Pokoj zostal zarezerwowany");
                    database.insertRoomBooking(new RoomBooking(0, idUser, idRoom, numberOfPeople, price, arrivaleDate, departureDate));
                    customTableModel.deleteRow(roomsTable.getSelectedRow());

                    HotelRoomBookingJoin h = new HotelRoomBookingJoin(roomReserveWindow.getName(), idUser, numberOfPeople,
                            price, arrivaleDate, departureDate);
                    BookingHistoryWindow.addDataRooms(h);

                    this.setVisible(false);
                    roomReserveWindow.setVisible(true);
                } else {
                    //JOptionPane.showMessageDialog(null, "You can find another room");
                }
            }
        });
    }

    public void updateTableModel(List<HotelRoomJoin> roomsList){
        customTableModel.update(roomsList);
        roomsList.forEach(r -> {
            hotelName2.setText(r.getName());
            city.setText(r.getCity());
            relaxationRoom2.setText(r.getRelaxationRoom().replace(",", " "));
            menuMeals.setText(r.getMenuMeals().replace(",", " "));
            menuDrinks.setText(r.getMenuDrinks().replace(",", " "));
            if(r.getParking() == true){
                isParking2.setText("PARKING");
            }
        });
    }
}