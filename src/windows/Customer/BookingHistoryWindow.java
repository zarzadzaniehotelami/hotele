package windows.Customer;

import classes.HotelConferenceRoomBookingJoin;
import classes.HotelRoomBookingJoin;
import classes.RoomBooking;
import classes.User;
import database.Database;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class BookingHistoryWindow extends JFrame {

    private Database database;
    private static BookingHistoryTableModel tableModel;
    private JTable historyTable;
    private JButton backButton;
    private static ConferenceRoomBookingHistoryTableModel conferenceRoomModel;
    private JTable conferenceRoomTable;
    private JLabel roomsLabel;
    private JLabel conferenceRoomLabel;
    private JButton deleteRoomButon;
    private JButton deleteConferenceRoomButton;

    private List<String> columnNames;
    private List<String> columnNamesConference;
    private List<HotelRoomBookingJoin> historyList;
    private List<HotelRoomBookingJoin> filteredHistoryList;
    private List<User> userList;
    private List<HotelConferenceRoomBookingJoin> conferenceRoomHistoryList;
    private List<HotelConferenceRoomBookingJoin> filteredConferenceRoomHistoryList;
    private List<RoomBooking> roomBookingList;


    private ClientAccountWindow clientAccountWindow;

    BookingHistoryWindow(ClientAccountWindow clientAccountWindow) {
        setSize(500, 400);
        setTitle("Bookings");
        //setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        this.clientAccountWindow = clientAccountWindow;

        CreateComponents();
        setVisible(true);
    }

    public void CreateComponents() {

        database = Database.getInstance();
        historyList = new LinkedList<>();
        historyList = database.innerJoinHotelRoomBooking();
        userList = new LinkedList<>();
        userList = database.selectUser();
        roomBookingList = new LinkedList<>();


        int idUser =  userList.stream().filter(u -> u.getLogin().equals(clientAccountWindow.getUsername())).map(u -> u.getIdUser()).findFirst().get();

        filteredHistoryList = historyList.stream()
                .filter(h -> h.getIdUser() == idUser).collect(Collectors.toList());

        conferenceRoomHistoryList = database.innerJoinHotelConferenceRoomBooking();
        filteredConferenceRoomHistoryList = conferenceRoomHistoryList.stream()
                .filter(h -> h.getIdUser() == idUser).collect(Collectors.toList());

        JPanel mainPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gb = new GridBagConstraints();

        JPanel firstPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gb1 = new GridBagConstraints();


        backButton = new JButton("Back");
        backButton.setPreferredSize(new Dimension(80, 30));
        backButton.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        gb1.gridx = 0;
        gb1.gridy = 0;
        gb1.anchor = GridBagConstraints.LINE_START;
        gb1.insets = new Insets(0,10,0,0);
        gb1.weightx = 1;
        gb1.weighty = 1;
        firstPanel.add(backButton, gb1);
        backButton.addActionListener(e -> {
            clientAccountWindow.setVisible(true);
            setVisible(false);
        });

        roomsLabel = new JLabel("ROOMS");
        roomsLabel.setFont(new Font("Monospace", Font.BOLD, 12));
        gb1.gridx = 0;
        gb1.gridy = 1;
        gb1.anchor = GridBagConstraints.LINE_START;
        gb1.insets = new Insets(15,10,5,0);
        gb1.weightx = 1;
        gb1.weighty = 1;
        firstPanel.add(roomsLabel, gb1);

        gb.gridx = 0;
        gb.gridy = 0;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(0,10,0,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(firstPanel, gb);

        columnNames = Arrays.asList("HOTEL", "ADULTS", "PRICE", "CHECK IN", "CHECK OUT");
        tableModel = new BookingHistoryTableModel(columnNames, filteredHistoryList);
        historyTable = new JTable(tableModel);
        historyTable.setPreferredScrollableViewportSize(new Dimension(400, 70));
        historyTable.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        JScrollPane scrollPane = new JScrollPane(historyTable);

        gb.gridx = 0;
        gb.gridy = 1;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(0,10,5,0);
        gb.weightx = 2;
        gb.weighty = 1;
        mainPanel.add(scrollPane, gb);

        roomBookingList = database.selectRoomBooking();

        deleteRoomButon = new JButton("DELETE RESERVATION");
        deleteRoomButon.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 10));
        gb.gridx = 0;
        gb.gridy = 2;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(0,10,5,0);
        gb.weightx = 2;
        gb.weighty = 1;
        mainPanel.add(deleteRoomButon, gb);
        deleteRoomButon.addActionListener(e-> {
            if(historyTable.getSelectedRow() == -1){
                JOptionPane.showMessageDialog(null, "Prosze wybrac rezerwacje do usuniecia");
            }
            else{
                tableModel.deleteRow(historyTable.getSelectedRow());

            }

        });

        conferenceRoomLabel = new JLabel("CONFERENCE ROOM");
        conferenceRoomLabel.setFont(new Font("Monospace", Font.BOLD, 12));
        gb.gridx = 0;
        gb.gridy = 3;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(20,10,5,0);
        gb.weightx = 1;
        gb.weighty = 1;
        mainPanel.add(conferenceRoomLabel, gb);


        columnNamesConference = Arrays.asList("HOTEL", "DATE");
        conferenceRoomModel = new ConferenceRoomBookingHistoryTableModel(columnNamesConference, filteredConferenceRoomHistoryList);
        conferenceRoomTable = new JTable(conferenceRoomModel);
        conferenceRoomTable.setPreferredScrollableViewportSize(new Dimension(400, 70));
        conferenceRoomTable.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 12));
        JScrollPane scrollPane1 = new JScrollPane(conferenceRoomTable);

        gb.gridx = 0;
        gb.gridy = 4;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(0,10,0,0);
        gb.weightx = 2;
        gb.weighty = 1;
        mainPanel.add(scrollPane1, gb);

        deleteConferenceRoomButton = new JButton("DELETE RESERVATION");
        deleteConferenceRoomButton.setFont(new Font("Monospace", Font.TRUETYPE_FONT, 10));
        gb.gridx = 0;
        gb.gridy = 5;
        gb.anchor = GridBagConstraints.LINE_START;
        gb.insets = new Insets(5,10,5,0);
        gb.weightx = 2;
        gb.weighty = 1;
        mainPanel.add(deleteConferenceRoomButton, gb);
        deleteConferenceRoomButton.addActionListener(e-> {
            if(conferenceRoomTable.getSelectedRow() == -1){
                JOptionPane.showMessageDialog(null, "Prosze wybrac rezerwacje do usuniecia");
            }
            else{
                conferenceRoomModel.deleteRow(conferenceRoomTable.getSelectedRow());
            }
        });

        add(mainPanel);
    }


    public static void addDataRooms(HotelRoomBookingJoin rowData){
        tableModel.addRow(rowData);
    }

    public static void addDataConferenceRoom(HotelConferenceRoomBookingJoin rowData){
        conferenceRoomModel.addRow(rowData);
    }
}
