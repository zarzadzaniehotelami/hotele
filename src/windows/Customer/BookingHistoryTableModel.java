package windows.Customer;

import classes.HotelRoomBookingJoin;
import classes.HotelRoomJoin;

import javax.swing.table.AbstractTableModel;
import java.time.LocalDate;
import java.util.List;

public class BookingHistoryTableModel  extends AbstractTableModel
 {
    private List<String> columnNames;
    private List<HotelRoomBookingJoin> historyList;

    public BookingHistoryTableModel(List<String> columnNames, List<HotelRoomBookingJoin> historyList)
    {
        this.columnNames = columnNames;
        this.historyList = historyList;
    }

    @Override
    public int getRowCount() {
        return historyList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        HotelRoomBookingJoin history = historyList.get(rowIndex);
        switch(columnIndex)
        {
            case 0: return history.getName();
            case 1: return history.getNumberOfPeople();
            case 2: return history.getPrice();
            case 3: return history.getArrivalDate();
            case 4: return history.getDepartureDate();
            default: return null;
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        HotelRoomBookingJoin history = historyList.get(rowIndex);
        switch(columnIndex)
        {
            case 0: history.setName(value.toString());
            case 1: history.setNumberOfPeople(Integer.parseInt(value.toString()));
            case 2: history.setPrice(Integer.parseInt(value.toString()));
            case 3: history.setArrivalDate(LocalDate.parse(value.toString()));
            case 4: history.setDepartureDate(LocalDate.parse(value.toString()));
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public void update(List<HotelRoomBookingJoin> roomList)
    {
        this.historyList = roomList;
        fireTableDataChanged();
    }

    public void addRow(HotelRoomBookingJoin rowData)
    {
        historyList.add(rowData);
        fireTableDataChanged();
    }

    public void deleteRow(int rowNumber)
    {
        historyList.remove(rowNumber);
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }
}
