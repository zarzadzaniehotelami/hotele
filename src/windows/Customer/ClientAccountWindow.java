package windows.Customer;

/**
 * Created by Artur on 25.04.17.
 */
import database.Database;
import windows.Admin.UserDataWindow;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class ClientAccountWindow extends JFrame {
    private JPanel ContentPanel;
    private String username;
    private Database db;

    private RoomReserveWindow roomReserveWindow;
    private BookingHistoryWindow bookingHistoryWindow;
    private ConferenceRoomReserveWindow conferenceRoomReserveWindow;

    public ClientAccountWindow(String _username, Database _db) {
        db=_db;
        if (_username==null) username="Undefined User";
        else username=_username;
        setTitle("User: " + username);
        setLocation(300, 300);
        setSize(400, 600);
        setLayout(new BorderLayout());
        this.getRootPane().setBorder(new LineBorder(new Color(15, 0, 255), 6));
        //setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        CreateComponents();
        pack();
        setVisible(true);

        roomReserveWindow = new RoomReserveWindow(this);
        roomReserveWindow.setVisible(false);

        bookingHistoryWindow = new BookingHistoryWindow(this);
        bookingHistoryWindow.setVisible(false);

        conferenceRoomReserveWindow = new ConferenceRoomReserveWindow(this);
        conferenceRoomReserveWindow.setVisible(false);
    }
    private void CreateComponents() {
        //main panel

        ContentPanel=new JPanel();
        ContentPanel.setLayout(new GridLayout(7, 1, 0, 10));
        ContentPanel.setBounds(20, 20, 320, 460);
        ContentPanel.setBackground(new Color(15, 0, 255));
        ContentPanel.setVisible(true);
        add(ContentPanel);

        //buttons
        JButton btnMyData=new JButton("Wyświetl moje dane");
        JButton btnModifyAccount=new JButton("Modyfikuj moje konto");
        JButton btnDeleteAccount=new JButton("Usuń moje konto");
        JButton btnReservationsHistory=new JButton("Historia rezerwacji");
        JButton btnReserveRoom=new JButton("Zarezerwuj pokój");
        JButton btnReserveHall=new JButton("Zarezerwuj salę");
        JButton btnLogout=new JButton("Wyloguj się");


        //buttons preferences

        ChangeButtonPreferences(btnMyData);
        ChangeButtonPreferences(btnModifyAccount);
        ChangeButtonPreferences(btnDeleteAccount);
        ChangeButtonPreferences(btnReservationsHistory);
        ChangeButtonPreferences(btnReserveRoom);
        ChangeButtonPreferences(btnReserveHall);
        ChangeButtonPreferences(btnLogout);

        //setting actions

        btnMyData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //show me my data
                new UserDataWindow(false, username, db);
            }
        });

        btnModifyAccount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //modify my account
                new UserDataWindow(true, username, db);
            }
        });

        btnDeleteAccount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //delete my account

            }
        });

        btnReservationsHistory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               bookingHistoryWindow.setVisible(true);
               setVisible(false);
            }
        });

        btnReserveRoom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                roomReserveWindow.setVisible(true);
                setVisible(false);
            }
        });

        btnReserveHall.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //reserve hall
                conferenceRoomReserveWindow.setVisible(true);
                setVisible(false);
            }
        });

        btnLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        //adding buttons to jpanel

        ContentPanel.add(btnMyData);
        ContentPanel.add(btnModifyAccount);
        ContentPanel.add(btnDeleteAccount);
        ContentPanel.add(btnReservationsHistory);
        ContentPanel.add(btnReserveRoom);
        ContentPanel.add(btnReserveHall);
        ContentPanel.add(btnLogout);
    }
    private void ChangeButtonPreferences(JButton btn) {
        btn.setSize(300, 50);
        btn.setBackground(new Color(0, 0, 100));
        btn.setForeground(new Color(234, 100, 50));
        btn.setFont(new Font("Arial Black", Font.PLAIN, 20));
    }

    public String getUsername() {
        return username;
    }

    public BookingHistoryWindow getBookingHistoryWindow() {
        return bookingHistoryWindow;
    }
}
