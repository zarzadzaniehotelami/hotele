package windows.Customer;

import classes.HotelConferenceRoomBookingJoin;

import javax.swing.table.AbstractTableModel;
import java.time.LocalDate;
import java.util.List;

public class ConferenceRoomBookingHistoryTableModel extends AbstractTableModel {
    private List<String> columnNames;
    private List<HotelConferenceRoomBookingJoin> historyList;

    public ConferenceRoomBookingHistoryTableModel(List<String> columnNames, List<HotelConferenceRoomBookingJoin> historyList)
    {
        this.columnNames = columnNames;
        this.historyList = historyList;
    }

    @Override
    public int getRowCount() {
        return historyList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        HotelConferenceRoomBookingJoin history = historyList.get(rowIndex);
        switch(columnIndex)
        {
            case 0: return history.getName();
            case 1: return history.getBookingDate();
            default: return null;
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        HotelConferenceRoomBookingJoin history = historyList.get(rowIndex);
        switch(columnIndex)
        {
            case 0: history.setName(value.toString());
            case 1: history.setBookingDate(LocalDate.parse(value.toString()));
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public void update(List<HotelConferenceRoomBookingJoin> roomList)
    {
        this.historyList = roomList;
        fireTableDataChanged();
    }

    public void addRow(HotelConferenceRoomBookingJoin rowData)
    {
        historyList.add(rowData);
        fireTableDataChanged();
    }

    public void deleteRow(int rowNumber)
    {
        historyList.remove(rowNumber);
        fireTableDataChanged();
    }

    @Override
    public boolean isCellEditable(int row, int col)
    {
        return false;
    }
}
