package windows.Employee;

import classes.RoomBooking;
import database.Database;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class RoomBookingsTable extends AbstractTableModel {

    private Database db;
    private List<RoomBooking> bookings;
    private int hotelID;
    private String[] names;

    public RoomBookingsTable(int hotelID, Database db) {
        this.db = db;
        this.hotelID = hotelID;
        getBookings();
        names = new String[7];
        names[0] = "ID";
        names[1] = "Rezerwujący";
        names[2] = "Numer pokoju";
        names[3] = "Ilość gości";
        names[4] = "Cena";
        names[5] = "Data przybycia";
        names[6] = "Data wyjazdu";
    }

    public void getBookings() {
        bookings = db.selectRoomBookingByHotelId(hotelID);
    }

    public int getRowCount() {
        return bookings.size();
    }

    public Object getValueAt(int row, int column) {
        switch(column) {
            case 0: bookings.get(row).getIdBookingRoom();
            case 1: db.selectUserById(bookings.get(row).getIdUser()).getSurname();
            case 2: db.selectRoomById(bookings.get(row).getIdRoom()).getRoomNumber();
            case 3: bookings.get(row).getNumberOfPeople();
            case 4: bookings.get(row).getPrice();
            case 5: bookings.get(row).getArrivalDate();
            case 6: bookings.get(row).getDepartureDate();
            default: return null;
        }
    }

    public String getColumnName(int ix) {
        return names[ix];
    }

    public int getColumnCount(){
        return 7;
    }

    public int getRoomBookingIdFromRow(int row) {
        return (int) getValueAt(row, 0);
    }

}
