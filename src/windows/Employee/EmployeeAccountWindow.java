package windows.Employee;

import database.Database;
import classes.Hotel;
import windows.Admin.*;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Set;

public class EmployeeAccountWindow extends JFrame {

    private Database db;
    private Hotel hotel;
    private JPanel MainPanel;

    public EmployeeAccountWindow(String hotelName, Database _db) {
        db = _db;
        SetHotel(hotelName);
        setTitle(hotel.getName());
        setLocation(300, 300);
        setSize(400, 600);
        setLayout(new BorderLayout());
        this.getRootPane().setBorder(new LineBorder(new Color(15, 0, 255), 6));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        CreateComponents();
        //pack();
        setVisible(true);
    }
    private void CreateComponents() {
        //main panel

        MainPanel = new JPanel();
        MainPanel.setLayout(new GridLayout(7, 1, 0, 10));
        MainPanel.setBounds(20, 20, 320, 460);
        MainPanel.setVisible(true);
        add(MainPanel);

        //buttons
        JButton hotelButton = new JButton("Szczegóły");
        hotelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new HotelsDataWindow(true, hotel.getIdHotel(), db);
            }
        });
        JButton bookingButton = new JButton("Rezerwacje");
        bookingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RoomBookingsWindow(hotel, db);
            }
        });
        JButton roomButton = new JButton("Pokoje");
        roomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RoomListWindow(hotel, db);
            }
        });
        JButton confButton = new JButton("Sala konferencyjna");
        confButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ConferenceRoomWindow(hotel, db);
            }
        });
        JButton logoutButton = new JButton("Wyloguj się");
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });


        //buttons preferences

        ChangeButtonPreferences(hotelButton);
        ChangeButtonPreferences(bookingButton);
        ChangeButtonPreferences(roomButton);
        ChangeButtonPreferences(confButton);
        ChangeButtonPreferences(logoutButton);

        //adding buttons to jpanel

        MainPanel.add(hotelButton);
        MainPanel.add(bookingButton);
        MainPanel.add(roomButton);
        MainPanel.add(confButton);
        MainPanel.add(logoutButton);
    }
    private void ChangeButtonPreferences(JButton btn) {
        btn.setSize(300, 50);
        btn.setFont(new Font("Arial Black", Font.PLAIN, 20));
    }

    private void SetHotel(String hotelName) {
        this.hotel = db.selectHotelByName(hotelName);
    }
}
