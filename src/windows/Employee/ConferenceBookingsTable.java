package windows.Employee;

import classes.ConferenceRoomBooking;
import database.Database;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ConferenceBookingsTable extends AbstractTableModel {

    private Database db;
    private List<ConferenceRoomBooking> bookings;
    private int hotelID;
    private String[] names;

    public ConferenceBookingsTable (int hotelID, Database db) {
        this.db = db;
        this.hotelID = hotelID;
        getBookings();
        names = new String[4];
        names[0] = "ID";
        names[1] = "Rezerwujący";
        names[2] = "Nazwa hotelu";
        names[3] = "Data";
    }

    public void getBookings() {
        bookings = db.selectConferenceRoomBookingByConferenceRoomId(db.selectHotelById(hotelID).getIdConferenceRoom());
    }

    public String getColumnName(int ix) {
        return names[ix];
    }

    public int getRowCount() {
        return bookings.size();
    }

    public Object getValueAt(int row, int column) {
        switch(column) {
            case 0: bookings.get(row).getIdConferenceRoomBooking();
            case 1: db.selectUserById(bookings.get(row).getIdUser()).getSurname();
            case 2: db.selectHotelById(hotelID).getName();
            case 3: bookings.get(row).getBookingDate();
            default: return null;
        }
    }

    public int getColumnCount(){
        return 4;
    }

}
