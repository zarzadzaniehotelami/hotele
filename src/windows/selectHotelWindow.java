package windows;

import classes.Hotel;
import database.Database;
import windows.Employee.EmployeeAccountWindow;
import windows.Employee.RoomListWindow;
import windows.Manager.ManagerAccountWindow;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class selectHotelWindow extends JFrame {

    private Database db;
    private JPanel MainPanel;
    private String[] names;
    private String status;

    public selectHotelWindow(String status, Database _db) {
//    public selectHotelWindow() {
        db = _db;
        this.status = status;
        setLocation(300, 300);
        setSize(400, 150);
        setLayout(new BorderLayout());
//        this.getRootPane().setBorder(new LineBorder(new Color(15, 0, 255), 6));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        CreateComponents();
        //pack();
        setVisible(true);
    }
    private void CreateComponents() {

        MainPanel = new JPanel();
        MainPanel.setLayout(new GridLayout(3, 1, 0, 30));
        MainPanel.setBounds(20, 20, 320, 460);
        MainPanel.setVisible(true);
        add(MainPanel);

        JComboBox<String> hotelList = new JComboBox<>(getHotelNames());
        hotelList.setSelectedIndex(0);

        MainPanel.add(hotelList);
        JButton selectButton = new JButton("select");
        selectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String msg = "";
                String name = "";
                if(hotelList.getSelectedIndex() != -1) {
                    msg = "You have selected hotel " + hotelList.getItemAt(hotelList.getSelectedIndex());
                    name = hotelList.getItemAt(hotelList.getSelectedIndex());
                }
                JOptionPane.showMessageDialog(null, msg);
                if (status.equals("Admin")) {
                    new RoomListWindow(GetHotel(name) ,db);
                }
                if(status.equals("Employee")) {
                    new EmployeeAccountWindow(name, db);
                    setVisible(false);
                }
                else if(status.equals("Manager"))
                    new ManagerAccountWindow(name, db);
            }
        });

        MainPanel.add(selectButton);
    }

    private void ChangeButtonPreferences(JButton btn) {
        btn.setSize(300, 50);
        btn.setFont(new Font("Arial Black", Font.PLAIN, 20));
    }

    private String[] getHotelNames() {
        names = new String[db.selectHotelsNames().size()];
        for(int i = 0; i < db.selectHotelsNames().size(); i++) {
            names[i] = db.selectHotelsNames().get(i);
            System.out.println(names[i]);
        }
        return names;
    }
    private Hotel GetHotel(String hotelName) {
        return db.selectHotelByName(hotelName);
    }
}
