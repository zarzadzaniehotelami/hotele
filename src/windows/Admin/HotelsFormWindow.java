package windows.Admin;

import classes.ConferenceRoom;
import classes.Hotel;
import database.Database;
import others.ValidationController;
import others.ValidationType;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HotelsFormWindow extends JFrame{
    private Database db;
    private JPanel ContentPanel;
    private JTextField fields[];
    private boolean ErrorDetected;
    private JComboBox jcbIsParking;
    private String IsParkingOption;
    private int errcode;
    public HotelsFormWindow(Database _db) {
        db=_db;
        ErrorDetected=false;
        errcode=0;
        setBounds(100, 100, 400, 800);
        setTitle("Hotel Form");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new FlowLayout());
        setVisible(true);
        ContentPanel=new JPanel();
        ContentPanel.setLayout(new GridLayout(9, 2));
        setBounds(110, 110, 500, 300);
        ContentPanel.setBorder(new LineBorder(Color.BLACK));
        ContentPanel.setVisible(true);
        setVisible(true);
        add(ContentPanel);
        PrepareComponents();
        pack();
    }


    public void paintComponent(Graphics g) {
        super.paintComponents(g);
    }

    public void PrepareComponents() {
        JLabel labels[]=new JLabel[8];
        fields=new JTextField[8];
        String options[]={"yes", "no"};
        jcbIsParking=new JComboBox(options);
        jcbIsParking.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox jcb=(JComboBox)e.getSource();
                IsParkingOption=(String)jcb.getSelectedItem();
                System.out.println(IsParkingOption);
            }
        });
        String labelnames[]={"name: *", "city: *", "number of rooms: *", "relaxation room: *", "menu meals: *", "menu drinks: *", "is parking: *", "conference room size: *"};
        for (int i=0; i<8; i++) {
            labels[i]=new JLabel(labelnames[i]);
            labels[i].setVisible(true);
            ContentPanel.add(labels[i]);
            if (i==6) {
                ContentPanel.add(jcbIsParking);
            }
            if (i<6) {
                fields[i]=new JTextField(20);
                fields[i].setVisible(true);
                ContentPanel.add(fields[i]);
            }
            if (i>6) {
                fields[i-1]=new JTextField(20);
                fields[i-1].setVisible(true);
                ContentPanel.add(fields[i-1]);
            }
        }
        JButton SubmitButton=new JButton("Enter");
        SubmitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkIfEmpty()) {
                    JOptionPane.showMessageDialog(null, "Wypełnij wszystkie pola");
                    errcode=3;
                }
                if (ValidationControl()==true && errcode!=3) {
                    SaveData();
                }
                else {
                    JOptionPane.showMessageDialog(null, "Incorrect data in text fields.");
                    switch(errcode) {
                        case 0 : JOptionPane.showMessageDialog(null, "Brak błędu"); break;
                        case 1 : JOptionPane.showMessageDialog(null, "Niektóre pola muszą zawierać tylko tekst"); break;
                        case 2 : JOptionPane.showMessageDialog(null, "Niektóre pola muszą zawierać tylko cyfry"); break;
                        default : break;
                    }
                }
            }
        });
        SubmitButton.setVisible(true);
        ContentPanel.add(SubmitButton);
    }


    private boolean checkIfEmpty() {
        for (int i=0; i<8; i++) {
            if (i<6) {
                if (fields[i].getText().equals("")) {
                    return true;
                }
            }
            if (i>6) {
                if (fields[i-1].getText().equals("")) {
                    return true;
                }
            }
        }
        return false;
    }

    public void SaveData() {
        ConferenceRoom cr=new ConferenceRoom(0, Integer.parseInt(fields[6].getText()));
        boolean v;
        if (IsParkingOption=="yes") v=true;
        else v=false;
        Hotel hotel=new Hotel(0, fields[0].getText(), fields[1].getText(), Integer.parseInt(fields[2].getText()), fields[3].getText(), fields[4].getText(), fields[5].getText(), v, cr.getIdConferenceRoom() );
        db.insertHotel(hotel);
        JOptionPane.showMessageDialog(null, "You have register hotel!");
        dispose();

    }

    public boolean ValidationControl() {
        ValidationController ControlFields=new ValidationController();
        for (int i=0; i<8; i++) {
            if (i<2) {
                ControlFields.SetStringAndValidationType(fields[i].getText(), ValidationType.INCLUDE_ONLY_LETTERS_AND_WHITESPACES);
                if (!ControlFields.Validate()) {
                    errcode=1;
                    return false;
                }
            }
            if (i==2 || i==7) {
                if (i==2) {
                    ControlFields.SetStringAndValidationType(fields[i].getText(), ValidationType.INCLUDE_ONLY_DIGITS);
                } else {
                    ControlFields.SetStringAndValidationType(fields[i-1].getText(), ValidationType.INCLUDE_ONLY_DIGITS);
                }
                if (!ControlFields.Validate()) {
                    errcode=2;
                    return false;
                }
            }
        }
        return true;
    }

}
