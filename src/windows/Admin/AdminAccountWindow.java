package windows.Admin;

import classes.Hotel;
import database.Database;
import windows.Employee.RoomListWindow;
import windows.selectHotelWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Artur on 16.05.17.
 */
public class AdminAccountWindow extends JFrame {
    private Database db;
    private JButton btnUsersManage;
    private JButton btnHotelsManage;
    private JButton btnRoomsManage;
    private JButton btnEmployee;
    private JButton btnManager;
    private JButton btnLogout;
    public AdminAccountWindow(Database _db) {
        db=_db;
        setTitle("Admin");
        setSize(300, 400);
        setLayout(new GridLayout(6, 1));
        //setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        CreateComponents();
        setVisible(true);
    }
    private void CreateComponents() {
        btnUsersManage=new JButton("Zarządzanie użytkownikami");
        btnUsersManage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UsersManageWindow(db);
            }
        });
        add(btnUsersManage);
        btnHotelsManage=new JButton("Zarządzanie hotelami");
        btnHotelsManage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new HotelsManageWindow(db);

            }
        });
        add(btnHotelsManage);
        btnRoomsManage=new JButton("Zarządzanie pokojami");
        btnRoomsManage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new selectHotelWindow("Admin", db);
            }
        });
        add(btnRoomsManage);
        btnEmployee=new JButton("Konto pracownika");
        btnEmployee.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new selectHotelWindow("Employee", db);
            }
        });
        add(btnEmployee);
        btnManager=new JButton("Konto kierownika");
        btnManager.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new selectHotelWindow("Manager", db);
            }
        });
        add(btnManager);
        btnLogout=new JButton("Wyloguj się");
        btnLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(btnLogout);
    }
}
