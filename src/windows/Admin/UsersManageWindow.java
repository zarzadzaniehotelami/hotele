package windows.Admin;

import database.Database;
import windows.Customer.ClientAccountWindow;
import windows.Customer.ClientFormWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Artur on 16.05.17.
 */
public class UsersManageWindow extends JFrame {
    private Database db;
    private UsersTable uTable;
    private JTable table;
    private int selected;
    public UsersManageWindow(Database _db) {
        selected=-1;
        db=_db;
        setTitle("Zarządzanie użytkownikami");
        setSize(600, 300);
        setLayout(new FlowLayout());
        CreateComponents();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
    private void CreateComponents() {
        JPanel Controller=new JPanel();
        Controller.setLayout(new FlowLayout());
        Controller.setSize(100, 200);
        JButton btnAddClient=new JButton("Dodaj Klienta");
        JButton btnDeleteClient=new JButton("Usuń Klienta");
        JButton btnModifyClient=new JButton("Modyfikuj Klienta");
        JButton btnAccessClientAccount=new JButton("Konto Klienta");
        JButton btnRefresh=new JButton("Odśwież");
        btnAddClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ClientFormWindow(db);
            }
        });
        btnDeleteClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected=table.getSelectedRow();
                if (selected!=-1) {
                    db.deleteUser(uTable.GetUserFromRow(selected).getIdUser());
                    uTable.LoadData();
                    table.repaint();
                    selected=-1;
                } else {
                    JOptionPane.showMessageDialog(null, "Choose record first");
                }
            }
        });
        btnModifyClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected=table.getSelectedRow();
                if (selected!=-1) {
                    new UserDataWindow(true, uTable.GetUserFromRow(selected).getLogin(), db);
                    table.repaint();
                    selected=-1;
                } else {
                    JOptionPane.showMessageDialog(null, "Choose record first");
                }
            }
        });
        btnAccessClientAccount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selected=table.getSelectedRow();
                if (selected!=-1) {
                    new ClientAccountWindow(uTable.GetUserFromRow(selected).getLogin(), db);
                    table.repaint();
                    selected=-1;
                } else {
                    JOptionPane.showMessageDialog(null, "Choose record first");
                }
            }
        });
        btnRefresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                uTable.LoadData();
                table.repaint();
            }
        });
        Controller.add(btnAddClient);
        Controller.add(btnDeleteClient);
        Controller.add(btnModifyClient);
        Controller.add(btnAccessClientAccount);
        Controller.add(btnRefresh);
        add(Controller);
        uTable=new UsersTable(db);
        table=new JTable(uTable);
        table.setPreferredSize(new Dimension(200, 400));
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrollpane=new JScrollPane(table);
        scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scrollpane);
    }
}
