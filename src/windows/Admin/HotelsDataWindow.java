package windows.Admin;

import classes.ConferenceRoom;
import classes.Hotel;
import database.Database;
import others.ValidationController;
import others.ValidationType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HotelsDataWindow extends JFrame{
    private int hotelId;
    private boolean CanBeModified;
    private Database db;
    private JTextField TName, TCity, TNumberRooms, TRelaxationRoom, Tmenumeals, Tmenudrinks, TSizeConferenceRoom;
    private JComboBox TIsParking;
    private String selected;

    public HotelsDataWindow(boolean _CanBeModified, int hotelId, Database _db) {
        selected="yes";
        db=_db;
        this.hotelId=hotelId;
        CanBeModified=_CanBeModified;
        setSize(350, 250);
        setTitle("Hotel");
        setLayout(new GridLayout(9, 1));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        CreateComponents();
        setVisible(true);
    }

    private void CreateComponents() {
        Hotel hotel=db.selectHotelById(hotelId);
        TName=new JTextField(20);
        TCity=new JTextField(20);
        TNumberRooms=new JTextField(20);
        TRelaxationRoom=new JTextField(20);
        Tmenumeals=new JTextField(20);
        Tmenudrinks=new JTextField(20);
        String options[]={"yes", "no"};
        TIsParking=new JComboBox(options);
        TSizeConferenceRoom=new JTextField(20);
        add(new JLabel("nazwa:"));
        if (CanBeModified) {

            TName.setText(hotel.getName());
            add(TName);
        } else {
            add(new JLabel(hotel.getName()));
        }
        add(new JLabel("miasto:"));
        if (CanBeModified) {
            TCity.setText(hotel.getCity());
            add(TCity);
        } else {
            add(new JLabel(hotel.getCity()));
        }
        add(new JLabel("number of rooms:"));
        if (CanBeModified) {
            TNumberRooms.setText(Integer.toString(hotel.getNumberOfRooms()));
            add(TNumberRooms);
        } else {
            add(new JLabel(Integer.toString(hotel.getNumberOfRooms())));
        }
        add(new JLabel("relaxation room:"));
        if (CanBeModified) {
            TRelaxationRoom.setText(hotel.getRelaxationRoom());
            add(TRelaxationRoom);
        } else {
            add(new JLabel(hotel.getRelaxationRoom()));
        }
        add(new JLabel("posiłki:"));
        if (CanBeModified) {
            Tmenumeals.setText(hotel.getMenuMeals());
            add(Tmenumeals);
        } else {
            add(new JLabel(hotel.getMenuMeals()));
        }
        add(new JLabel("napoje:"));
        if (CanBeModified) {
            Tmenudrinks.setText(hotel.getMenuDrinks());
            add(Tmenudrinks);
        } else {
            add(new JLabel(hotel.getMenuDrinks()));
        }
        add(new JLabel("możliwość parkowania:"));
        if (CanBeModified) {
            TIsParking.setSelectedIndex(0);
            TIsParking.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JComboBox jcb=(JComboBox)e.getSource();
                    selected=(String)jcb.getSelectedItem();
                }
            });
            add(TIsParking);
        } else {
            if (hotel.getIsParking()==true) {
                add(new JLabel("Yes"));
            } else {
                add(new JLabel("No"));
            }
        }

        add(new JLabel("rozmiar sali konferencyjnej:"));
        if (CanBeModified) {
            ConferenceRoom cr=db.selectConferenceRoomById(hotel.getIdConferenceRoom());
            TSizeConferenceRoom.setText(Integer.toString(cr.getSize()));
            add(TSizeConferenceRoom);
        } else {
            ConferenceRoom cr=db.selectConferenceRoomById(hotel.getIdConferenceRoom());
            add(new JLabel(Integer.toString(cr.getSize())));
        }

        JButton btnEnter=new JButton("Save");
        btnEnter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (TName.getText().equals("") || TCity.getText().equals("") || TNumberRooms.getText().equals("") || TRelaxationRoom.getText().equals("") || Tmenumeals.getText().equals("") || Tmenudrinks.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Fill all fields (*) ...");
                    return;
                }
                if (ValidationControl()) {
                    boolean v;
                    if (selected=="yes") v=true;
                    else v=false;
                    ConferenceRoom cr=new ConferenceRoom(0, Integer.parseInt(TSizeConferenceRoom.getText()));
                    db.insertConferenceRoom(cr);
                    Hotel h=new Hotel(hotel.getIdHotel(), TName.getText(), TCity.getText(), Integer.parseInt(TNumberRooms.getText()), TRelaxationRoom.getText(), Tmenumeals.getText(), Tmenudrinks.getText(), v, cr.getIdConferenceRoom());
                    db.updateHotel(h);
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrect data in text fields.");
                }
            }
        });
        if (!CanBeModified) btnEnter.setEnabled(false);
        add(btnEnter);
        JButton btnOk=new JButton("Ok");
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(btnOk);
    }
    public boolean ValidationControl() {
        ValidationController ControlFields=new ValidationController();
        ControlFields.SetStringAndValidationType(TName.getText(), ValidationType.INCLUDE_ONLY_LETTERS_AND_WHITESPACES);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TCity.getText(), ValidationType.INCLUDE_ONLY_LETTERS_AND_WHITESPACES);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TRelaxationRoom.getText(), ValidationType.INCLUDE_ONLY_LETTERS_AND_WHITESPACES);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(Tmenumeals.getText(), ValidationType.INCLUDE_ONLY_LETTERS_AND_WHITESPACES);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(Tmenudrinks.getText(), ValidationType.INCLUDE_ONLY_LETTERS_AND_WHITESPACES);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TNumberRooms.getText(), ValidationType.INCLUDE_ONLY_DIGITS);
        if (!ControlFields.Validate()) {
            return false;
        }
        ControlFields.SetStringAndValidationType(TSizeConferenceRoom.getText(), ValidationType.INCLUDE_ONLY_DIGITS);
        if (!ControlFields.Validate()) {
            return false;
        }
        return true;
    }

}
