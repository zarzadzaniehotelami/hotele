package database;

import classes.*;

import java.sql.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

public class Database {
    private static Connection connection;
    private static Statement statement;
    private static Database instance;


    private Database()
    {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:Baza.db");
            statement = connection.createStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        createTable();
    }

    public static Database getInstance()
    {
        if(instance == null)
        {
            instance = new Database();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }

    public void createTable()  {

        String userTable = "CREATE TABLE IF NOT EXISTS User " +
                "(idUser INTEGER PRIMARY KEY AUTOINCREMENT, name Varchar(50), surname VARCHAR(50), login VARCHAR(50), password VARCHAR(50), " +
                "phoneNumber VARCHAR(50), emailAddress VARCHAR(50), status VARCHAR(50));";

        String conferenceRoomTable = "CREATE TABLE IF NOT EXISTS ConferenceRoom (idConferenceRoom INTEGER PRIMARY KEY AUTOINCREMENT, size INTEGER)";

        String hotelTable = "CREATE TABLE IF NOT EXISTS Hotel (idHotel INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(50), city VARCHAR(50), numberOfRooms INTEGER, relaxationRoom VARCHAR(50), menuMeals VARCHAR(50), menuDrinks VARCHAR(50), isParking BOOLEAN, idConferenceRoom INTEGER, FOREIGN KEY (idConferenceRoom) REFERENCES ConferenceRoom(IdConferenceRoom) ON DELETE CASCADE ON UPDATE CASCADE);";

        String roomTable = "CREATE TABLE IF NOT EXISTS Room (idRoom INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "idHotel INTEGER, " +
                "roomNumber INTEGER, " +
                "numberOfPeople INTEGER, " +
                "price INTEGER, " +
                "isAvailable BOOLEAN, " +
                "FOREIGN KEY (idHotel) REFERENCES Hotel(IdHotel) ON DELETE CASCADE ON UPDATE CASCADE);";

        String conferenceRoomBooking = "CREATE TABLE IF NOT EXISTS ConferenceRoomBooking (idConferenceRoomBooking INTEGER PRIMARY KEY AUTOINCREMENT, idUser INTEGER, idConferenceRoom INTEGER, bookingDate DATE, FOREIGN KEY (idUser) REFERENCES User(IdUser) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (idConferenceRoom) REFERENCES ConferenceRoom(idConferenceRoom) ON DELETE CASCADE ON UPDATE CASCADE);";

        String roomBooking = "CREATE TABLE IF NOT EXISTS RoomBooking (idRoomBooking INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "idUser INTEGER, " +
                "idRoom INTEGER, " +
                "numberOfPeople INTEGER, " +
                "price DOUBLE, " +
                "arrivalDate DATE, " +
                "departureDate DATE, " +
                "FOREIGN KEY (idUser) REFERENCES User(IdUser) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (idRoom) REFERENCES Room(idRoom) ON DELETE CASCADE ON UPDATE CASCADE);";

        String emplyeeTable = "CREATE TABLE IF NOT EXISTS Employee" +
                "(idEmployee INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(50), " +
                "surname VARCHAR(50), " +
                "login VARCHAR(50), " +
                "password VARCHAR(50), " +
                "emailAddress VARCHAR(50), " +
                "status VARCHAR(50), " +
                "idHotel INTEGER, " +
                "FOREIGN KEY (idHotel) REFERENCES Hotel(IdHotel) ON DELETE CASCADE ON UPDATE CASCADE);";

        try {
            statement.execute(userTable);
            statement.execute(conferenceRoomTable);
            statement.execute(hotelTable);
            statement.execute(roomTable);
            statement.execute(conferenceRoomBooking);
            statement.execute(roomBooking);
            statement.execute(emplyeeTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void insertUser(User user)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO User (idUser, name, surname, login, password, phoneNumber, emailAddress, status) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?);");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhoneNumber());
            preparedStatement.setString(6, user.getEmailAddress());
            preparedStatement.setString(7, user.getStatus());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User user) {
        String query;
        try {
            query="UPDATE User SET name=?, surname=?, login=?, password=?, phoneNumber=?, emailAddress=? WHERE idUser=?;";
            PreparedStatement preparedStatement=connection.prepareStatement(query);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhoneNumber());
            preparedStatement.setString(6, user.getEmailAddress());
            preparedStatement.setInt(7, user.getIdUser());
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(int idUser)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM User WHERE idUser = ?;");
            preparedStatement.setInt(1, idUser);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> selectUser()
    {
        List<User> userList = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM User");
            while(resultSet.next()){
                int idUser = resultSet.getInt("idUser");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                String phoneNumber = resultSet.getString("phoneNumber");
                String emailAddress = resultSet.getString("emailAddress");
                String status = resultSet.getString("status");
                userList.add(new User(idUser, name, surname, login, password, phoneNumber, emailAddress, status));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

    public User selectUserByLogin(String Login) {
        String query;
        User user;
        try {
            query="SELECT * FROM User WHERE login=?;";
            PreparedStatement preparedStatement=connection.prepareStatement(query);
            preparedStatement.setString(1, Login);
            ResultSet resultSet=preparedStatement.executeQuery();
            user=new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8));
            return user;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insertConferenceRoom(ConferenceRoom conferenceRoom)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO ConferenceRoom (idConferenceRoom, size) VALUES (NULL, ?);");
            preparedStatement.setInt(1, conferenceRoom.getSize());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteConferenceRoom(int idConferenceRoom)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM ConferenceRoom WHERE idConferenceRoom = ?;");
            preparedStatement.setInt(1, idConferenceRoom);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<ConferenceRoom> selectConferenceRoom()
    {
        List<ConferenceRoom> conferenceRoomList = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM ConferenceRoom");
            while(resultSet.next()){
                int idConferenceRoom = resultSet.getInt("idConferenceRoom");
                int size = resultSet.getInt("size");

                conferenceRoomList.add(new ConferenceRoom(idConferenceRoom, size));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conferenceRoomList;
    }

    public void insertHotel(Hotel hotel)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Hotel (idHotel, name, city, numberOfRooms, " +
                    "relaxationRoom, menuMeals, menuDrinks, isParking, idConferenceRoom) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?);");
            preparedStatement.setString(1, hotel.getName());
            preparedStatement.setString(2, hotel.getCity());
            preparedStatement.setInt(3, hotel.getNumberOfRooms());
            preparedStatement.setString(4, hotel.getRelaxationRoom());
            preparedStatement.setString(5, hotel.getMenuMeals());
            preparedStatement.setString(6, hotel.getMenuDrinks());
            preparedStatement.setBoolean(7, hotel.getIsParking());
            preparedStatement.setInt(8, hotel.getIdConferenceRoom());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteHotel(int idHotel)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM Hotel WHERE idHotel = ?;");
            preparedStatement.setInt(1, idHotel);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Hotel> selectHotel()
    {
        List<Hotel> hotelList = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Hotel");
            while(resultSet.next()){
                int idHotel = resultSet.getInt("idHotel");
                String name = resultSet.getString("name");
                String city = resultSet.getString("city");
                int numberOfRooms = resultSet.getInt("numberOfRooms");
                String relaxationRoom = resultSet.getString("relaxationRoom");
                String menuMeals = resultSet.getString("menuMeals");
                String menuDrinks = resultSet.getString("menuDrinks");
                Boolean isParking = resultSet.getBoolean("isParking");
                int idConferenceRoom = resultSet.getInt("idConferenceRoom");
                hotelList.add(new Hotel(idHotel, name, city, numberOfRooms, relaxationRoom,
                        menuMeals, menuDrinks, isParking, idConferenceRoom));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hotelList;
    }

    public void updateHotel(Hotel hotel) {
        String query;
        try {
            query="UPDATE Hotel SET name=?, city=?, numberOfRooms=?, relaxationRoom=?, menuMeals=?, menuDrinks=?, isParking=?, idConferenceRoom=? WHERE idHotel=?;";
            PreparedStatement preparedStatement=connection.prepareStatement(query);
            preparedStatement.setString(1, hotel.getName());
            preparedStatement.setString(2, hotel.getCity());
            preparedStatement.setInt(3, hotel.getNumberOfRooms());
            preparedStatement.setString(4, hotel.getRelaxationRoom());
            preparedStatement.setString(5, hotel.getMenuMeals());
            preparedStatement.setString(6, hotel.getMenuDrinks());
            preparedStatement.setBoolean(7, hotel.getIsParking());
            preparedStatement.setInt(8, hotel.getIdConferenceRoom());
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public Hotel selectHotelById(int id)
    {
        String query;
        Hotel hotel;
        try {
            if(id==0)
                return null;
            else {
                query = "SELECT * FROM Hotel WHERE idHotel=?;";

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                hotel = new Hotel(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getBoolean(8),
                        resultSet.getInt(9));
                return hotel;
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

        public Hotel selectHotelByName(String Name)
    {
        String query;
        Hotel hotel;
        try {
            if(Name=="")
                return null;
            else {
                query = "SELECT * FROM Hotel WHERE name=?;";

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, Name);
                ResultSet resultSet = preparedStatement.executeQuery();

                hotel = new Hotel(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getBoolean(8),
                        resultSet.getInt(9));
                return hotel;
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> selectHotelsNames(){
        List<String> hotels = new ArrayList<String>();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT name FROM Hotel");
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                String name = rs.getString(1);
                hotels.add(name);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return hotels;

    }

    public void insertRoom(Room room)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Room (idRoom, idHotel, roomNumber, numberOfPeople, " +
                    "price, isAvailable) VALUES (NULL, ?, ?, ?, ?, ?);");
            preparedStatement.setInt(1, room.getIdHotel());
            preparedStatement.setInt(2, room.getRoomNumber());
            preparedStatement.setInt(3, room.getNumberOfPeople());
            preparedStatement.setInt(4, room.getPrice());
            preparedStatement.setBoolean(5, room.getIfAvailable());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteRoom(int idRoom)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM Room WHERE idRoom = ?;");
            preparedStatement.setInt(1, idRoom);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Room> selectRoom()
    {
        List<Room> roomList = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Room");
            while(resultSet.next()){
                int idRoom = resultSet.getInt("idRoom");
                int idHotel = resultSet.getInt("idHotel");
                int roomNumber = resultSet.getInt("roomNumber");
                int numberOfPeople = resultSet.getInt("numberOfPeople");
                int price = resultSet.getInt("price");
                Boolean isAvailable = resultSet.getBoolean("isAvailable");
                roomList.add(new Room(idRoom, idHotel, roomNumber, numberOfPeople, price, isAvailable));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roomList;
    }

    public void insertConferenceRoomBooking(ConferenceRoomBooking conferenceRoomBooking)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO ConferenceRoomBooking (idConferenceRoomBooking, idUser, idConferenceRoom, " +
                    " bookingDate) VALUES (NULL, ?, ?, ?);");
            preparedStatement.setInt(1, conferenceRoomBooking.getIdUser());
            preparedStatement.setInt(2, conferenceRoomBooking.getIdConferenceRoom());
            preparedStatement.setDate(3, Date.valueOf(conferenceRoomBooking.getBookingDate()));
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteConferenceRoomBooking(int idConferenceRoomBooking)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM ConferenceRoomBooking WHERE idConferenceRoomBooking = ?;");
            preparedStatement.setInt(1, idConferenceRoomBooking);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<ConferenceRoomBooking> selectConferenceRoomBooking()
    {
        List<ConferenceRoomBooking> conferenceRoomBookingList = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM ConferenceRoomBooking");
            while(resultSet.next()){
                int idConferenceRoomBooking = resultSet.getInt("idConferenceRoomBooking");
                int idUser = resultSet.getInt("idUser");
                int idConferenceRoom = resultSet.getInt("idConferenceRoom");
                LocalDate bookingDate = resultSet.getDate("bookingDate").toLocalDate();
                conferenceRoomBookingList.add(new ConferenceRoomBooking(idConferenceRoomBooking, idUser, idConferenceRoom, bookingDate));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conferenceRoomBookingList;
    }

    public void insertRoomBooking(RoomBooking roomBooking)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO RoomBooking (idRoomBooking, idUser, idRoom, " +
                    " numberOfPeople, price, arrivalDate, departureDate) VALUES (NULL, ?, ?, ?, ?, ?, ?);");
            preparedStatement.setInt(1, roomBooking.getIdUser());
            preparedStatement.setInt(2, roomBooking.getIdRoom());
            preparedStatement.setInt(3, roomBooking.getNumberOfPeople());
            preparedStatement.setDouble(4, roomBooking.getPrice());
            preparedStatement.setDate(5, Date.valueOf(roomBooking.getArrivalDate()));
            preparedStatement.setDate(6, Date.valueOf(roomBooking.getDepartureDate()));
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<RoomBooking> selectRoomBooking()
    {
        List<RoomBooking> roomBookingList = new LinkedList<>();
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM RoomBooking");
            while(resultSet.next()){
                int idRoomBooking = resultSet.getInt("idRoomBooking");
                int idUser = resultSet.getInt("idUser");
                int idRoom = resultSet.getInt("idRoom");
                int numberOfPeople = resultSet.getInt("numberOfPeople");
                double price = resultSet.getDouble("price");
                LocalDate arrivalDate = resultSet.getDate("arrivalDate").toLocalDate();
                LocalDate departureDate = resultSet.getDate("depatrureDate").toLocalDate();

                roomBookingList.add(new RoomBooking(idRoomBooking, idUser, idRoom, numberOfPeople, price, arrivalDate, departureDate));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roomBookingList;
    }

    public void deleteRoomBooking(int idRoomBooking)
    {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM RoomBooking WHERE idRoomBooking = ?;");
            preparedStatement.setInt(1, idRoomBooking);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<HotelRoomJoin> innerJoinHotelRoom()
    {
        List<HotelRoomJoin> joinList = new LinkedList<>();

        try {

            ResultSet result = statement.executeQuery("SELECT Hotel.name, Hotel.city, Hotel.relaxationRoom, Hotel.menuMeals," +
                    "Hotel.menuDrinks, Hotel.isParking, Room.idRoom, Room.roomNumber, Room.numberOfPeople, Room.isAvailable, Room.price " +
                    "FROM Hotel INNER JOIN Room ON Hotel.idHotel = Room.idHotel");

            while (result.next())
            {
                String name = result.getString("name");
                String city = result.getString("city");
                String relaxationRoom = result.getString("relaxationRoom");
                String menuMeals = result.getString("menuMeals");
                String menuDrinks = result.getString("menuDrinks");
                Boolean isParking = result.getBoolean("isParking");
                int idRoom = result.getInt("idRoom");
                int roomNumber = result.getInt("roomNumber");
                int numberOfPeople = result.getInt("numberOfPeople");
                Boolean isAvailable = result.getBoolean("isAvailable");
                int price = result.getInt("price");

                joinList.add(new HotelRoomJoin(name, city, relaxationRoom, menuMeals, menuDrinks, isParking, idRoom,
                        roomNumber, numberOfPeople, isAvailable, price));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return joinList;
    }

    public List<Room> selectRoomByHotelId(int idH) {
        List<Room> rooms = new LinkedList<>();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM Room WHERE IdHotel=?;");
            ps.setInt(1,idH);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                int roomId = rs.getInt("idRoom");
                int hotelID = rs.getInt("idHotel");
                int roomNumber = rs.getInt("roomNumber");
                int NumberOfPeople = rs.getInt("numberOfPeople");
                int Price = rs.getInt("price");
                boolean isAvailable = rs.getBoolean("isAvailable");
                rooms.add(new Room(roomId,hotelID, roomNumber, NumberOfPeople, Price,isAvailable));
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rooms;
    }

    public List<RoomBooking> selectRoomBookingByHotelId(int id) {
        List<RoomBooking> bookings = new LinkedList<>();

        try{
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM RoomBooking " +
                    "WHERE idRoom IN (SELECT idRoom FROM Room WHERE idHotel=?);");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                int idRoomBooking = rs.getInt("idRoomBooking");
                int idUser = rs.getInt("idUser");
                int idRoom = rs.getInt("idRoom");
                int nbOfPpl = rs.getInt("numberOfPeople");
                double price = rs.getDouble("price");
                LocalDate arrival = rs.getDate("arrivalDate").toLocalDate();
                LocalDate dep = rs.getDate("departureDate").toLocalDate();

                bookings.add(new RoomBooking(idRoomBooking,idUser,idRoom,nbOfPpl,price,arrival,dep));
            }
        }catch(SQLException ex) {
            ex.printStackTrace();
        }

        return bookings;

    }


    public List<HotelRoomBookingJoin> innerJoinHotelRoomBooking()
    {
        List<HotelRoomBookingJoin> joinList = new LinkedList<>();
        try {
            ResultSet result = statement.executeQuery("SELECT Hotel.name, RoomBooking.idUser, RoomBooking.numberOfPeople, RoomBooking.price, " +
                    "RoomBooking.arrivalDate, RoomBooking.departureDate  FROM Hotel INNER JOIN Room ON Hotel.idHotel = Room.idHotel" +
                    " INNER JOIN RoomBooking ON Room.idRoom = RoomBooking.idRoom");
            while (result.next())
            {
                String name = result.getString("name");
                int idUser = result.getInt("idUser");
                int numberOfPeople = result.getInt("numberOfPeople");
                int price = result.getInt("price");
                LocalDate arrivalDate = result.getDate("arrivalDate").toLocalDate();
                LocalDate departureDate = result.getDate("isdepartureDate").toLocalDate();

                joinList.add(new HotelRoomBookingJoin(name, idUser, numberOfPeople, price, arrivalDate, departureDate));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return joinList;
    }

    public List<HotelConferenceRoomBookingJoin> innerJoinHotelConferenceRoomBooking()
    {
        List<HotelConferenceRoomBookingJoin> joinList = new LinkedList<>();
        try {
            ResultSet result = statement.executeQuery("SELECT Hotel.name, ConferenceRoomBooking.idUser, ConferenceRoomBooking.bookingDate" +
                    " FROM Hotel INNER JOIN ConferenceRoomBooking ON Hotel.idConferenceRoom = ConferenceRoomBooking.idConferenceRoom");
            while (result.next())
            {
                String name = result.getString("name");
                int idUser = result.getInt("idUser");
                LocalDate bookingDate = result.getDate("bookingDate").toLocalDate();

                joinList.add(new HotelConferenceRoomBookingJoin(name, idUser, bookingDate));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return joinList;
    }



    public List<HotelConferenceRoomJoin> innerJoinHotelConferenceRoom()
    {
        List<HotelConferenceRoomJoin> joinList = new LinkedList<>();
        try {
            ResultSet result = statement.executeQuery("SELECT Hotel.name, Hotel.city, ConferenceRoom.size" +
                    " FROM Hotel INNER JOIN ConferenceRoom ON Hotel.idConferenceRoom = ConferenceRoom.idConferenceRoom");
            while (result.next())
            {
                String name = result.getString("name");
                String city = result.getString("city");
                int size = result.getInt("size");

                joinList.add(new HotelConferenceRoomJoin(name, city, size));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return joinList;
    }

    public ConferenceRoom selectConferenceRoomById(int id) {
        String query = "";
        ConferenceRoom cr = null;
        try {
            if(id==0)
                return null;
            else {
                query = "SELECT * FROM ConferenceRoom WHERE idConferenceRoom=?;";

                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                cr=new ConferenceRoom(id, resultSet.getInt(1));
                return cr;
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void insertEmployee(Employee employee) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Employee (idEmployee, name, surname, login, password, emailAddress, status, idHotel) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?);");
            preparedStatement.setString(1, employee.getName());
            preparedStatement.setString(2, employee.getSurname());
            preparedStatement.setString(3, employee.getLogin());
            preparedStatement.setString(4, employee.getPassword());
            preparedStatement.setString(5, employee.getEmailAddress());
            preparedStatement.setString(6, employee.getStatus());
            preparedStatement.setInt(7, employee.getHotelId());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public Employee selectEmployeeByLogin(String login) {
        Employee employee;
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM Employee WHERE login=?;");
            preparedStatement.setString(1, login);
            ResultSet resultSet=preparedStatement.executeQuery();
            employee=new Employee(resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getString(7),
                    resultSet.getInt(8));
            return employee;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getUserStatus(String login) {
        String status = "";
        try {
            String query = "SELECT status FROM User WHERE login LIKE ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1,login);
            ResultSet rs = ps.executeQuery();
            status = rs.getString(1);
            return status;
        } catch (SQLException e) {
            //e.printStackTrace();
            status="";
        }

        return status;
    }

    public User selectUserById(int id) {
        User user;
        try {
            String query = "SELECT * FROM User WHERE idUser=?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            user=new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8));
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Room selectRoomById(int id) {
        Room room;
        try {
            String query = "SELECT * FROM Room WHERE idRoom=?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            int roomId = rs.getInt("idRoom");
                int hotelID = rs.getInt("idHotel");
                int roomNumber = rs.getInt("roomNumber");
                int NumberOfPeople = rs.getInt("numberOfPeople");
                int Price = rs.getInt("price");
                boolean isAvailable = rs.getBoolean("isAvailable");
                room = new Room(roomId,hotelID, roomNumber, NumberOfPeople, Price,isAvailable);
            return room;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ConferenceRoomBooking> selectConferenceRoomBookingByConferenceRoomId(int id) {
        List<ConferenceRoomBooking> bookings = new LinkedList<>();

        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM ConferenceRoomBooking WHERE idConferenceRoom=?");
            while (rs.next()) {
                bookings.add(new ConferenceRoomBooking(
                        rs.getInt("idConferenceRoomBooking"),
                        rs.getInt("idUser"),
                        rs.getInt("idConferenceRoom"),
                        rs.getDate("bookingDate").toLocalDate()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookings;
    }

}
