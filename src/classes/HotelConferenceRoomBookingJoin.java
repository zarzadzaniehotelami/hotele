package classes;

import java.time.LocalDate;

public class HotelConferenceRoomBookingJoin {
    private String name;
    private int idUser;
    private LocalDate bookingDate;

    public HotelConferenceRoomBookingJoin(String name, int idUser, LocalDate bookingDate) {
        this.name = name;
        this.idUser = idUser;
        this.bookingDate = bookingDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate = bookingDate;
    }
}
