package classes;


public class Employee {
    private int idEmployee;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String emailAddress;
    private String status;
//    private String hotelName;
    private int idHotel;

    public Employee(int idEmployee, String name, String surname, String login, String password, String emailAddress, String status, int idHotel) {
        this.idEmployee = idEmployee;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.emailAddress = emailAddress;
        this.status = status;
//        this.hotelName = hotelName;
        this.idHotel = idHotel;
    }

    public int getIdEmployee() {
        return idEmployee;
    }

    public void setId(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getHotelId() {
        return idHotel;
    }

    public void setHotelId(int hotelId) {
        this.idHotel = hotelId;
    }

    //public void setHotelId(String hotelName) {
    // find hotelId by Name
    // setHotelId(int hotelId)
    // }

    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idEmployee +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", status='" + status + '\'' +
                ", hotelID='" + idHotel +'\'' +
                '}';
    }


}
