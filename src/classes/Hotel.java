package classes;


public class Hotel {
    private int idHotel;
    private String name;
    private String city;
    private int numberOfRooms;
    private String relaxationRoom;
    private String menuMeals;
    private String menuDrinks;
    private boolean isParking;
    private int idConferenceRoom;
    //private int idMenu;


    public Hotel(int idHotel, String name, String city, int numberOfRooms, String relaxationRoom, String menuMeals, String menuDrinks, boolean isParking, int idConferenceRoom) {
        this.idHotel = idHotel;
        this.name = name;
        this.city = city;
        this.numberOfRooms = numberOfRooms;
        this.relaxationRoom = relaxationRoom;
        this.menuMeals = menuMeals;
        this.menuDrinks = menuDrinks;
        this.isParking = isParking;
        this.idConferenceRoom = idConferenceRoom;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public int getIdConferenceRoom() {
        return idConferenceRoom;
    }

    public void setIdConferenceRoom(int idConferenceRoom) {
        this.idConferenceRoom = idConferenceRoom;
    }

    public String getRelaxationRoom() {
        return relaxationRoom;
    }

    public void setRelaxationRoom(String relaxationRoom) {
        this.relaxationRoom = relaxationRoom;
    }

    public boolean getIsParking() {
        return isParking;
    }

    public void setParking(boolean parking) {
        isParking = parking;
    }

    public String getMenuMeals() {
        return menuMeals;
    }

    public void setMenuMeals(String menuMeals) {
        this.menuMeals = menuMeals;
    }

    public String getMenuDrinks() {
        return menuDrinks;
    }

    public void setMenuDrinks(String menuDrinks) {
        this.menuDrinks = menuDrinks;
    }



    @Override
    public String toString() {
        return "Hotel{" +
                "idHotel=" + idHotel +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", numberOfRooms=" + numberOfRooms +
                ", relaxationRoom='" + relaxationRoom + '\'' +
                ", menuMeals='" + menuMeals + '\'' +
                ", menuDrinks='" + menuDrinks + '\'' +
                ", getIsParking=" + isParking +
                ", idConferenceRoom=" + idConferenceRoom +
                '}';
    }
}
