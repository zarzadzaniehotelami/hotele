package classes;

import java.time.LocalDate;

public class HotelRoomBookingJoin {
    private String name;
    private int idUser;
    private int numberOfPeople;
    private double price;
    private LocalDate arrivalDate;
    private LocalDate departureDate;

    public HotelRoomBookingJoin(String name, int idUser, int numberOfPeople, double price, LocalDate arrivalDate, LocalDate departureDate) {
        this.name = name;
        this.idUser = idUser;
        this.numberOfPeople = numberOfPeople;
        this.price = price;
        this.arrivalDate = arrivalDate;
        this.departureDate = departureDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
