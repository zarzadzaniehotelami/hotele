package classes;

public class ConferenceRoom {

    private int idConferenceRoom;
    private int size;

    public ConferenceRoom(int idConferenceRoom, int size) {
        this.idConferenceRoom = idConferenceRoom;
        this.size = size;
    }

    public int getIdConferenceRoom() {
        return idConferenceRoom;
    }

    public void setIdConferenceRoom(int idConferenceRoom) {
        this.idConferenceRoom = idConferenceRoom;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ConferenceRoom{" +
                "idConferenceRoom=" + idConferenceRoom +
                ", size=" + size +
                '}';
    }
}
