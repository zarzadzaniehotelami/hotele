package classes;

import java.time.LocalDate;

public class ConferenceRoomBooking {
    private int idConferenceRoomBooking;
    private int idUser;
    private int idConferenceRoom;
    private LocalDate bookingDate;

    public ConferenceRoomBooking(int idConferenceRoomBooking, int idUser, int idConferenceRoom, LocalDate bookingDate) {
        this.idConferenceRoomBooking = idConferenceRoomBooking;
        this.idUser = idUser;
        this.idConferenceRoom = idConferenceRoom;
        this.bookingDate = bookingDate;
    }

    public int getIdConferenceRoomBooking() {
        return idConferenceRoomBooking;
    }

    public void setIdConferenceRoomBooking(int idConferenceRoomBooking) {
        this.idConferenceRoomBooking = idConferenceRoomBooking;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdConferenceRoom() {
        return idConferenceRoom;
    }

    public void setIdConferenceRoom(int idConferenceRoom) {
        this.idConferenceRoom = idConferenceRoom;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate = bookingDate;
    }

    @Override
    public String toString() {
        return "ConferenceRoomBooking{" +
                "idConferenceRoomBooking=" + idConferenceRoomBooking +
                ", idUser=" + idUser +
                ", idConferenceRoom=" + idConferenceRoom +
                ", bookingDate=" + bookingDate +
                '}';
    }
}
